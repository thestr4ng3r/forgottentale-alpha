#ifndef __TMS_H
#define __TMS_H

class CMeshPosition;
class CIdlePosition;
class CCustomPosition;
class CAnimation;
struct CKeyFrame;
class CMesh;
struct CTriangle;
struct CMaterial;
struct CVertex;

#include "tresources.h"

#include "vector2.h"
#include "vector.h"
#include "utils.h"
#include "buffers.h"
#include "shader.h"
#include "face_shader.h"
#include "texture.h"
#include "vertex.h"
#include "triangle.h"
#include "material.h"
#include "mesh.h"
#include "position.h"
#include "animation.h"

#endif
