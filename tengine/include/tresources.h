//-----------------------------------------------------------------------
// This file has been generated by stringres by Metallic Entertainment
// for further information go to http://www.metallic-entertainment.com
//-----------------------------------------------------------------------

#ifndef _TRESOURCES_H
#define _TRESOURCES_H

extern const char *face_shader_vert;
extern const char *face_shader_frag;

#endif