#version 130

in vec3 Pos;
in vec3 Normal;
in vec4 TexCoord;
in vec2 NormalTexCoord0;
in vec2 NormalTexCoord1;
in vec3 CamDir;
in vec4 ScreenPos;

out vec4 gl_FragColor;

uniform float Anim;

uniform sampler2D Texture;
uniform sampler2D NormalTex;

uniform sampler2D ColorTex;
uniform sampler2D DepthTex;

float Height(vec2 offset)
{
	vec2 r;
	r.x = sin((Pos.x + offset.x) * 10.0);
	r.y = sin((Pos.z + offset.y) * 10.0);
	return r.x + r.y;
}

vec2 HeightNormal(float offset)
{
	vec2 p, n;
	float m;
	p = vec2(Height(vec2(offset, 0.0)), Height(vec2(0.0, offset)));
	n = vec2(Height(vec2(-offset, 0.0)), Height(vec2(0.0, -offset)));
	m = Height(vec2(0.0, 0.0));
	vec2 d;
	d.x = (p.x - m) - (n.x - m);
	d.y = (p.y - m) - (n.y - m);
	return d;
}

vec2 Offset(void)
{
	vec3 normal0 = normalize(texture2D(NormalTex, NormalTexCoord0.xy + vec2(0.5, 0.5) * Anim * 0.6).xyz - vec3(0.5, 0.5, 0.5)) * 0.7;
	vec3 normal1 = normalize(texture2D(NormalTex, NormalTexCoord1.xy + vec2(0.5, 0.5) * Anim * 0.45).xyz - vec3(0.5, 0.5, 0.5)) * 2.0;
	return normal0.xy + normal1.xy;
}

void main(void)
{
	vec3 tex_coord = TexCoord.xyz / TexCoord.w;
	vec2 offset = (Offset() * 0.01) / TexCoord.z;
	//offset += vec2(0.1, 0.1) * Anim;
	tex_coord += vec3(offset, 0.0) * 8.0; //vec3(HeightNormal(0.0003), 0.0);
	vec3 color = texture2D(Texture, tex_coord.st).rgb * 0.5;
		
	vec3 screen_coord = ScreenPos.xyz / ScreenPos.w;
	float depth_delta = texture2D(DepthTex, screen_coord.xy).r - screen_coord.z;
	depth_delta *= ScreenPos.w;
	depth_delta *= 199.9;
	vec2 bg_coord = screen_coord.xy + offset * 8.0;
	color += texture2D(ColorTex, bg_coord.xy).rgb * 0.5;
	float alpha = clamp(depth_delta, 0.0, 0.5) / 0.5;
	color = mix(texture2D(ColorTex, screen_coord.xy).rgb, color, alpha);
	
	gl_FragColor = vec4(color, 1.0);
}