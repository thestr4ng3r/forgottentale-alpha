#version 130

in vec4 gl_Vertex;
in vec3 gl_Normal;

uniform mat4 gl_TextureMatrix[gl_MaxTextureCoords];
uniform mat4 gl_ModelViewProjectionMatrix;
uniform mat4 gl_ModelViewMatrix;
uniform mat4 gl_ModelViewMatrixInverse;

out vec3 Pos;
out vec3 Normal;
out vec4 TexCoord;
out vec2 NormalTexCoord0;
out vec2 NormalTexCoord1;
out vec3 CamDir;
out vec4 ScreenPos;

void main(void)
{
	Normal = normalize(gl_Normal.xyz);
	
	vec4 final_pos = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = final_pos;
	ScreenPos = gl_TextureMatrix[7] * gl_Vertex;
	TexCoord = gl_TextureMatrix[6] * gl_Vertex;
	NormalTexCoord0 = gl_Vertex.xz * 2.0 * 0.6;
	NormalTexCoord1 = gl_Vertex.xz * 0.35 * 0.6;
	Pos = gl_Vertex.xyz;
	vec4 camera_OS = gl_ModelViewMatrixInverse * vec4(0.0, 0.0, 0.0, 1.0);
	CamDir = normalize(camera_OS.xyz * gl_Vertex.w - gl_Vertex.xyz * camera_OS.w);
}