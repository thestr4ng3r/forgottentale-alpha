#version 130

in vec2 UVCoord;

out vec4 gl_FragColor;

uniform sampler2D Texture;
uniform sampler2D DepthTexture;

uniform float DepthThreshold;
uniform float Gray;

void main(void)
{
	vec4 depth_color = texture2D(DepthTexture, UVCoord); 
	float depth = depth_color.r;
	vec3 color;
	vec2 dx = dFdx(UVCoord);
	vec2 dy = dFdy(UVCoord);
	
	float radius = length(UVCoord.xy - vec2(0.5, 0.5));
	if(radius > 0.5)
	{
		float rd = radius - 0.5;
		rd /= 0.7;
		dx += vec2(1.0, 1.0) * rd * 0.005;
		dy += vec2(1.0, 1.0) * rd * 0.005;
	}
	float darkness = 0.0;
	if(radius > 0.5)
		darkness += (radius - 0.5) * 1.0;
	
	/*if(!(depth < DepthThreshold || depth == 1.0))
	{
		dx += depth - DepthThreshold;
		dy += depth - DepthThreshold;
	}*/
	
	color = textureGrad(Texture, UVCoord, dx, dy).rgb;
	color = mix(color, vec3(1.0/3.0, 1.0/3.0, 1.0/3.0) * (color.r + color.g + color.b), Gray);
	color -= vec3(0.8, 1.0, 1.0) * darkness;
	gl_FragColor = vec4(color, 1.0);
}