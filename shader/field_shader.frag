#define MAX_TOWERS 100
#define MAX_WP 100

uniform vec3 Towers[MAX_TOWERS];
uniform float TowersRange[MAX_TOWERS];
uniform int TowersCount;

varying vec3 Pos;

float DistToLine(vec3 p, vec3 a, vec3 b, float ml)
{
		vec3 n = normalize(b-a);
		float l = dot(p, n) - dot(n, a);
		l /= dot(n*n, vec3(1.0, 1.0, 1.0));
		if(l<0.0 || l>length(b-a))
			return min(length(p-a), length(p-b));
		vec3 f = a + n*l;
		return length(f-p);
}

void main(void)
{
	vec3 color = vec3(1.0, 1.0, 1.0);
   int i;
	int towers_count = 0;
	bool walkable = true;
	for(i=0; i<TowersCount; i++)
	{
		if(length(Pos-Towers[i]) < TowersRange[i])
		{
			towers_count++;
			walkable = false;
		}
		if(length(Pos-Towers[i]) < TowersRange[i] + 1.0)
			walkable = false;
	}
	if(!walkable)
		color.r = 0.7;
	if(towers_count >= 2)
		color.r = 0.1;
	float alpha = 1.0;
	gl_FragColor = vec4(color, alpha);
}