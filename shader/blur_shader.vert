#version 130

in vec2 gl_MultiTexCoord0;
uniform mat4 gl_ModelViewProjectionMatrix;
in vec4 gl_Vertex;

out vec2 UVCoord;

void main(void)
{
	UVCoord = gl_MultiTexCoord0.xy;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
