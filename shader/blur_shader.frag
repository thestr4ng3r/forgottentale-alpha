#version 130

#define BLUR_HORIZONTAL  0
#define BLUR_VERTICAL    1

out vec4 gl_FragColor;

uniform sampler2D Texture;

in vec2 UVCoord;

uniform int BlurDirection;

uniform float TargetWidth;
uniform float TargetHeight;
uniform float BlurSize;

void main()
{
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	int i;
	
	if(BlurDirection == BLUR_VERTICAL)
	{
		float blur_size = BlurSize / TargetWidth;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y - 4.0*blur_size)) * 0.05;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y - 3.0*blur_size)) * 0.09;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y - 2.0*blur_size)) * 0.12;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y - blur_size)) * 0.15;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y)) * 0.16;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y + blur_size)) * 0.15;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y + 2.0*blur_size)) * 0.12;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y + 3.0*blur_size)) * 0.09;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y + 4.0*blur_size)) * 0.05;
	}
	else if(BlurDirection == BLUR_HORIZONTAL)
	{
		float blur_size = BlurSize / TargetHeight;
		color += texture2D(Texture, vec2(UVCoord.x - 4.0*blur_size, UVCoord.y)) * 0.05;
		color += texture2D(Texture, vec2(UVCoord.x - 3.0*blur_size, UVCoord.y)) * 0.09;
		color += texture2D(Texture, vec2(UVCoord.x - 2.0*blur_size, UVCoord.y)) * 0.12;
		color += texture2D(Texture, vec2(UVCoord.x - blur_size, UVCoord.y)) * 0.15;
		color += texture2D(Texture, vec2(UVCoord.x, UVCoord.y)) * 0.16;
		color += texture2D(Texture, vec2(UVCoord.x + blur_size, UVCoord.y)) * 0.15;
		color += texture2D(Texture, vec2(UVCoord.x + 2.0*blur_size, UVCoord.y)) * 0.12;
		color += texture2D(Texture, vec2(UVCoord.x + 3.0*blur_size, UVCoord.y)) * 0.09;
		color += texture2D(Texture, vec2(UVCoord.x + 4.0*blur_size, UVCoord.y)) * 0.05;
	}
	color.a = 1.0;
	gl_FragColor = color;
}