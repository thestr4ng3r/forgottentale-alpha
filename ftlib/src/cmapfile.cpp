#include "ftlib.h"


CVector MapVec3(CVector2 v)
{
	return Vec(v.x, 0.0, -v.y);
}

CVector2 MapVec2(CVector v)
{
	return Vec(v.x, -v.z);
}

CMapFile::CMapFile(void)
{
	start = end = Vec(0.0, 0.0);
}

int CMapFile::LoadFromFile(const char *file)
{
	float minx = INFINITY;
	float miny = INFINITY;
	float maxx = -INFINITY;
	float maxy = -INFINITY;
	vector<CSimpleTriangle>::iterator i;
	int vi;
	CSimpleTriangle *t;
	xmlDocPtr doc;
	xmlNodePtr cur;

	ground.clear();
	water.clear();
	start = end = Vec(0.0, 0.0);

	doc = xmlParseFile(file);

	if(!doc)
		return 1;

	cur = xmlDocGetRootElement(doc);

	if(!cur)
		return 2;

	if(xmlStrcmp(cur->name, (const xmlChar *)"map"))
		return 3;

	cur = cur->children;
	while(cur)
	{
		if(!xmlStrcmp(cur->name, (const xmlChar *)"mesh"))
			ParseMeshNode(doc, cur);
		else if(!xmlStrcmp(cur->name, (const xmlChar *)"ground"))
			ParseTriangleGroupNode(cur, ground);
		else if(!xmlStrcmp(cur->name, (const xmlChar *)"water"))
			ParseTriangleGroupNode(cur, water);
		else if(!xmlStrcmp(cur->name, (const xmlChar *)"start"))
			start = ParseVertex2Node(cur);
		else if(!xmlStrcmp(cur->name, (const xmlChar *)"end"))
			end = ParseVertex2Node(cur);

		cur = cur->next;
	}

	// Calculate Min and Max of the ground

	for(i=ground.begin(); i!=ground.end(); i++)
	{
		t = &((CSimpleTriangle &)*i);
		for(vi=0; vi<3; vi++)
		{
			if(t->v[vi].x < minx)
				minx = t->v[vi].x;
			if(t->v[vi].z < miny)
				miny = t->v[vi].z;
			if(t->v[vi].x > maxx)
				maxx = t->v[vi].x;
			if(t->v[vi].z > maxy)
				maxy = t->v[vi].z;
		}
	}

	if(minx == INFINITY)
		minx = -INFINITY;
	if(miny == INFINITY)
		miny = -INFINITY;
	if(maxx == INFINITY)
		maxx = -INFINITY;
	if(maxy == INFINITY)
		maxy = -INFINITY;

	ground_min = Vec(minx, miny);
	ground_max = Vec(maxx, maxy);

	return 0;
}

int CMapFile::SaveToFile(const char *file)
{
	xmlDocPtr doc;
	xmlNodePtr root;
	xmlNodePtr node;
	xmlChar *xmlbuff;
	int buffersize;
	vector<CSimpleTriangle>::iterator i;

	doc = xmlNewDoc((const xmlChar *)"1.0");
	root = xmlNewNode(0, (const xmlChar *)"map");
	xmlDocSetRootElement(doc, root);

	node = xmlNewNode(0, (const xmlChar *)"mesh");
	xmlNodeSetContent(node, (const xmlChar *)mesh_file);
	xmlAddChild(root, node);

	if(water.size() > 0)
	{
		node = CreateTriangleGroupNode(water, "water");
		xmlAddChild(root, node);
	}

	node = CreateTriangleGroupNode(ground, "ground");
	xmlAddChild(root, node);

	xmlAddChild(root, CreateVertex2Node(start, "start"));
	xmlAddChild(root, CreateVertex2Node(end, "end"));

	xmlDocDumpFormatMemory(doc, &xmlbuff, &buffersize, 1);

	xmlFree(xmlbuff);

	int ret = xmlSaveFormatFile(file, doc, 1);

	xmlFreeDoc(doc);

	return ret;
}


/*
Example File:

<?xml version="1.0"?>
<map maxx="10.0" minx="-10.0" maxy="10.0" miny="-10.0">
	<mesh>mesh.tms</mesh>
	<water>
		<triangle>
			<vertex x="1.0" y="0.0" z="0.0" />
			<vertex x="0.0" y="0.0" z="-1.0" />
			<vertex x="1.0" y="0.0" z="-1.0" />
		</triangle>
	</water>
	<ground>
		<triangle>
			<vertex x="10.0" y="0.0" z="10.0" />
			<vertex x="-10.0" y="0.0" z="-10.0" />
			<vertex x="10.0" y="0.0" z="-10.0" />
		</triangle>
		<triangle>
			<vertex x="10.0" y="0.0" z="10.0" />
			<vertex x="-10.0" y="0.0" z="-10.0" />
			<vertex x="10.0" y="0.0" z="-10.0" />
		</triangle>
	</ground>
	<way>
		<vertex x="-8.0" y="8.0" />
		<vertex x="-5.0" y="0.0" />
		<vertex x="8.0" y="-8.0" />
	</way>
</map>
  */

void CMapFile::ParseMeshNode(xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c;
	char *f;

	c = xmlNodeListGetString(doc, cur->children, 1);
	f = new char[strlen((char *)c) + 1];
	strcpy(f, (char *)c);
	mesh_file = f;
}



int LoadWayFromFile(const char *file, vector<CVector2> &vec)
{
	float values[6];
	char read_values[100];
	int value_count = 0;
	int eof = 0;
	int char_count = 0;
	char read_char;
	int handle = open(file, O_RDONLY);

	if(!handle)
		return 0;

	while(!eof)
	{
		if(read(handle, &read_char, 1) == 0)
			eof = 1;
		if(!eof)
		{
			if(read_char == ' ' || read_char == '\n')
				continue;
			if(read_char == '#')
				while(1)
				{
					if(!read(handle, &read_char, 1))
					{
						eof = 1;
						break;
					}
					if(read_char == '\n')
					{
						break;
					}
				}
			if(eof)
				break;
		}
		if(read_char == ',' || eof)
		{
			values[value_count] = atof(read_values);
			char_count = 0;
			value_count++;
			if(value_count == 6)
			{
				vec.push_back(Vec(values[0], values[2]));
				value_count = 0;
			}
			continue;
		}
		read_values[char_count] = read_char;
		char_count++;
		if(char_count<=100)
			read_values[char_count] = 0;
	}
	close(handle);
	return 1;
}

CVector ParseVertexNode(xmlNodePtr cur)
{
	CVector r;
	r.x = atof((const char *)xmlGetProp(cur, (const xmlChar *)"x"));
	r.y = atof((const char *)xmlGetProp(cur, (const xmlChar *)"y"));
	r.z = atof((const char *)xmlGetProp(cur, (const xmlChar *)"z"));
	return r;
}

CVector2 ParseVertex2Node(xmlNodePtr cur)
{
	CVector2 r;
	r.x = atof((const char *)xmlGetProp(cur, (const xmlChar *)"x"));
	r.y = atof((const char *)xmlGetProp(cur, (const xmlChar *)"y"));
	return r;
}

CSimpleTriangle ParseTriangleNode(xmlNodePtr cur)
{
	CSimpleTriangle r;
	int i;

	r.v[0] = r.v[1] = r.v[2] = Vec(0.0, 0.0, 0.0);
	cur = cur->children;
	i=0;
	while(cur && i<3)
	{
		if(!xmlStrcmp(cur->name, (const xmlChar *)"vertex"))
		{
			r.v[i] = ParseVertexNode(cur);
			i++;
		}
		cur = cur->next;
	}
	return r;
}

void ParseTriangleGroupNode(xmlNodePtr cur, vector<CSimpleTriangle> &vec)
{
	cur = cur->children;
	while(cur)
	{
		if(!xmlStrcmp(cur->name, (const xmlChar *)"triangle"))
			vec.push_back(ParseTriangleNode(cur));
		cur = cur->next;
	}
}

void ParseVertexGroupNode(xmlNodePtr cur, vector<CVector> &vec)
{
	cur = cur->children;
	while(cur)
	{
		if(!xmlStrcmp(cur->name, (const xmlChar *)"vertex"))
			vec.push_back(ParseVertexNode(cur));
		cur = cur->next;
	}
}

void ParseVertex2GroupNode(xmlNodePtr cur, vector<CVector2> &vec)
{
	CVector2 v;
	cur = cur->children;
	while(cur)
	{
		if(!xmlStrcmp(cur->name, (const xmlChar *)"vertex"))
		{
			v = ParseVertex2Node(cur);
			vec.push_back(v);
		}

		cur = cur->next;
	}
}


void ScaleVertexGroup(vector<CVector> &vec, CVector v)
{
	vector<CVector>::iterator i;

	for(i=vec.begin(); i!=vec.end(); i++)
	{
		((CVector &)*i) *= v;
	}
}


void ScaleVertex2Group(vector<CVector2> &vec, CVector2 v)
{
	vector<CVector2>::iterator i;

	for(i=vec.begin(); i!=vec.end(); i++)
	{
		((CVector2 &)*i) *= v;
	}
}

void ScaleTriangleGroup(vector<CSimpleTriangle> &vec, CVector v)
{
	vector<CSimpleTriangle>::iterator i;
	CSimpleTriangle *t;

	for(i=vec.begin(); i!=vec.end(); i++)
	{
		t = &(CSimpleTriangle &)*i;
		t->v[0] *= v;
		t->v[1] *= v;
		t->v[2] *= v;
	}
}

xmlNodePtr CreateTriangleNode(CSimpleTriangle t)
{
	xmlNodePtr root, vert;
	int i;

	root = xmlNewNode(0, (const xmlChar *)"triangle");
	for(i=0; i<3; i++)
	{
		vert = xmlNewNode(0, (const xmlChar *)"vertex");
		xmlNewProp(vert, (const xmlChar *)"x", (const xmlChar *)ftoa(t.v[i].x));
		xmlNewProp(vert, (const xmlChar *)"y", (const xmlChar *)ftoa(t.v[i].y));
		xmlNewProp(vert, (const xmlChar *)"z", (const xmlChar *)ftoa(t.v[i].z));
		xmlAddChild(root, vert);
	}

	return root;
}

xmlNodePtr CreateTriangleGroupNode(vector<CSimpleTriangle> v, const char *name)
{
	xmlNodePtr root;
	vector<CSimpleTriangle>::iterator i;
	CSimpleTriangle t;

	root = xmlNewNode(0, (const xmlChar *)name);

	for(i=v.begin(); i!=v.end(); i++)
	{
		t = (CSimpleTriangle &)*i;
		xmlAddChild(root, CreateTriangleNode(t));
	}

	return root;
}

xmlNodePtr CreateVertex2GroupNode(vector<CVector2> v, const char *name)
{
	xmlNodePtr root, vert;
	CVector2 vt;
	vector<CVector2>::iterator i;

	root = xmlNewNode(0, (const xmlChar *)name);
	for(i=v.begin(); i!=v.end(); i++)
	{
		vert = xmlNewNode(0, (const xmlChar *)"vertex");
		vt = (CVector2 &)*i;
		xmlNewProp(vert, (const xmlChar *)"x", (const xmlChar *)ftoa(vt.x));
		xmlNewProp(vert, (const xmlChar *)"y", (const xmlChar *)ftoa(vt.y));
		xmlAddChild(root, vert);
	}

	return root;
}


xmlNodePtr CreateVertex2Node(CVector2 v, const char *name)
{
	xmlNodePtr vert;

	vert = xmlNewNode(0, (const xmlChar *)name);
	xmlNewProp(vert, (const xmlChar *)"x", (const xmlChar *)ftoa(v.x));
	xmlNewProp(vert, (const xmlChar *)"y", (const xmlChar *)ftoa(v.y));

	return vert;
}





















