
#include "ftlib.h"

CSimpleTriangle2 CSimpleTriangle::Components(int a, int b, float ma, float mb)
{
	CSimpleTriangle2 r;
	int i;

	for(i=0; i<3; i++)
		r.v[i] = Vec(v[i].v[a] * ma, v[i].v[b] * mb);

	return r;
}

int VecOverTriangle(CVector2 v, CSimpleTriangle2 t)
{
	int c = 0;
	int i;
	CVector2 a, b;
	CVector2 bigger, smaller;

	for(i=0; i<3; i++)
	{
		a = t.v[i];
		b = t.v[(i+1)%3];
		if(a.y > b.y)
		{
			bigger = a;
			smaller = b;
		}
		else
		{
			bigger = b;
			smaller = a;
		}
		if(smaller.y > v.y || bigger.y < v.y)
			continue;
		if((smaller.x + ((v.y - smaller.y) / (bigger.y - smaller.y)) * (bigger.x - smaller.x)) <= v.x)
			c++;
	}

	if(c==1)
		return 1;
	return 0;
}

/*int VecOverTriangle(CVector2 v, CSimpleTriangle2 t)
{
	int i, c;
	CVector2 a, b, d;
	CVector2 max, min;
	float yd;
	float temp;

	temp = t.v[0].x > t.v[1].x ? t.v[0].x : t.v[1].x;
	max.x = temp > t.v[2].x ? temp : t.v[2].x;
	temp = t.v[0].y > t.v[1].y ? t.v[0].y : t.v[1].y;
	max.y = temp > t.v[2].y ? temp : t.v[2].y;
	temp = t.v[0].x < t.v[1].x ? t.v[0].x : t.v[1].x;
	min.x = temp < t.v[2].x ? temp : t.v[2].x;
	temp = t.v[0].y < t.v[1].y ? t.v[0].y : t.v[1].y;
	min.y = temp < t.v[2].y ? temp : t.v[2].y;

	if(v.x < min.x || v.x > max.x || v.y < min.y || v.y > max.y)
		return 0;

	c = 0;
	for(i=0; i<3; i++)
	{
		a = t.v[i];
		b = t.v[i == 3 ? 0 : 1];
		d = b - a;
		if((a.y > v.y && b.y > v.y) || (a.y < v.y && b.y < v.y))
			continue;
		yd = v.y - a.y;
		d.Normalize();
		d *= (yd / d.y);
		a += d;
		if(a.x <= v.x)
			c++;
	}

	if(floor((double)c / 2.0) != ceil((double)c / 2.0))
		return 1;
	return 0;
}*/

void ConvertMeshToTriangles(CMesh *mesh, vector<CSimpleTriangle> &vec)
{
	CTriangle *t;
	int j;
	CSimpleTriangle *st;
	int i;

	for(j=0; j<mesh->GetTriangleCount(); j++)
	{
		t = mesh->GetTriangle(j);
		st = new CSimpleTriangle();
		for(i=0; i<3; i++)
			memcpy(st->v[i].v, ((CVector *)t->v[i])->v, sizeof(float) * 3);
		vec.push_back(*st);
	}
}
