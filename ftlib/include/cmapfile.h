#ifndef _CMAPFILE_H
#define _CMAPFILE_H

#include <iostream>
#include <vector>

#include <libxml/tree.h>

#include "towerengine.h"

using namespace std;


struct CMapFile
{
	char *mesh_file;

	CVector2 start, end;
	CVector2 ground_min, ground_max;

	vector<CSimpleTriangle> ground;
	vector<CSimpleTriangle> water;

	CMapFile(void);

	int LoadFromFile(const char *file);
	int SaveToFile(const char *file);
	void ParseMeshNode(xmlDocPtr doc, xmlNodePtr cur);
};

int LoadWayFromFile(const char *file, vector<CVector2> &vec);

CVector MapVec3(CVector2 v);
CVector2 MapVec2(CVector v);

CVector ParseVertexNode(xmlNodePtr cur);
CVector2 ParseVertex2Node(xmlNodePtr cur);
CSimpleTriangle ParseTriangleNode(xmlNodePtr cur);
void ParseTriangleGroupNode(xmlNodePtr cur, vector<CSimpleTriangle> &vec);
void ParseVertexGroupNode(xmlNodePtr cur, vector<CVector> &vec);
void ParseVertex2GroupNode(xmlNodePtr cur, vector<CVector2> &vec);
void ScaleVertexGroup(vector <CVector> &vec, CVector v);
void ScaleVertex2Group(vector <CVector2> &vec, CVector2 v);
void ScaleTriangleGroup(vector <CSimpleTriangle> &vec, CVector v);

xmlNodePtr CreateTriangleNode(CSimpleTriangle t);
xmlNodePtr CreateTriangleGroupNode(vector<CSimpleTriangle> v, const char *name);
xmlNodePtr CreateVertex2GroupNode(vector<CVector2> v, const char *name);
xmlNodePtr CreateVertex2Node(CVector2 v, const char *name);

#endif // CMAPFILE_H
