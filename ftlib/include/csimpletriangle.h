
#ifndef _CSIMPLETRIANGLE_H
#define _CSIMPLETRIANGLE_H

struct CSimpleTriangle2
{
	CVector2 v[3];

	CSimpleTriangle2(CVector2 a, CVector2 b, CVector2 c) { v[0] = a; v[1] = b; v[2] = c; };
	CSimpleTriangle2(void) { v[0] = v[1] = v[2] = Vec(0.0, 0.0); };
};

struct CSimpleTriangle
{
	CVector v[3];

	CSimpleTriangle(CVector a, CVector b, CVector c) { v[0] = a; v[1] = b; v[2] = c; };
	CSimpleTriangle(void) { v[0] = v[1] = v[2] = Vec(0.0, 0.0, 0.0); };

	CSimpleTriangle2 Components(int a, int b, float ma = 1.0, float mb = 1.0);
	CSimpleTriangle2 Top2(void) { return Components(0, 2, 1.0, -1.0); };
	CSimpleTriangle2 Top(void) { return Components(0, 2); };

	CVector Normal(void) { CVector n = Cross(v[1] - v[0], v[2] - v[0]); n.Normalize(); return n; };
};

int VecOverTriangle(CVector2 v, CSimpleTriangle2 t);
void ConvertMeshToTriangles(CMesh *mesh, vector<CSimpleTriangle> &vec);

#endif
