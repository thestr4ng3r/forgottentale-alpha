MAKEFLAGS += -swr

all: forgotten_tale

run:
	@./run_forgotten_tale.sh

start: forgotten_tale ftlib towerengine

forgotten_tale: forgotten_tale/bin/forgotten_tale
	@echo "Allright, Forgotten Tale has been built successfully!"
	@echo "type make run to run the game!"

forgotten_tale/bin/forgotten_tale: ftlib towerengine
	@./make_forgotten_tale.sh

ftlib: ftlib/lib/ftlib.a
	@echo "FTLib has been built successfully."

ftlib/lib/ftlib.a: towerengine
	@./make_ftlib.sh

towerengine: tengine/lib/libtowerengine.a
	@echo "TowerEngine has been built successfully."

tengine/lib/libtowerengine.a:
	@./make_towerengine.sh

clean:
	@./make_towerengine.sh clean
	@./make_ftlib.sh clean
	@./make_towerengine.sh clean

distclean:
	@./make_towerengine.sh distclean
	@./make_ftlib.sh distclean
	@./make_forgotten_tale.sh distclean

