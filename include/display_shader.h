#ifndef _DISPLAY_SHADER_H
#define _DISPLAY_SHADER_H

class CDisplayShader : public CShader
{
	private:
		GLint color_tex_uniform;
		GLint depth_tex_uniform;
		GLint depth_threshold_uniform;
		GLint gray_uniform;

	public:
		void Init(void);
		void SetTexture(GLuint color, GLuint depth, float thresh);
		void SetTexCoord(CVector2 c);
		void SetGray(float g);
};

extern CDisplayShader display_shader;

#endif

