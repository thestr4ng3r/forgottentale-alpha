#ifndef _CENEMY_H
#define _CENEMY_H

struct CEnemy : CFlamable
{
	//CGame *game;
	CEnemy *chain_next;

	CMeshHandler *mesh;
	
	CWay *way;
	CWay *way_temp;

	static const float fade_dist = 1.0;

	bool on_ground;

	float way_dist;
	float speed;
	float slow_speed;
	float real_speed;
	float life;
	float slow_time;
	float stunned_time;
	int slowed;
	int stunned;
	float max_life;
	int dead;
	int to_delete;
	int run;
	float burn_time;
	int burning;
	CVector pos;
	CVector aim_pos;
	CVector2 rot;
	CVector dir;

	CEnemy(CGame *game);
	~CEnemy(void);

	float GetAlpha(void);
	void CalculatePosition(void);
	void Run(float time);
	void PutToGL();
};

#endif
