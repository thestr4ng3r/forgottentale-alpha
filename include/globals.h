#ifndef _GLOBALS_H
#define _GLOBALS_H

extern int display_width;
extern int display_height;

extern int fullscreen;
extern int aa_level;
 
extern const SDL_VideoInfo *video_info;
extern SDL_Surface *screen;
//extern GLuint gl_screen;
extern SDL_Rect screen_rect;

extern int audio_freq; 
extern Uint16 audio_format;
extern int audio_channels;
extern int audio_buffers;

extern CMusicPlayer *music_player;

extern OGLFT::Translucent *fonts[FONT_COUNT];

extern SDL_Surface *cursor, *rotate_cursor, *move_cursor;
extern GLuint cursor_gl, rotate_cursor_gl, move_cursor_gl;

extern CProgram *program;

#endif
