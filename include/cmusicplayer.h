#ifndef _CMUSICPLAYER_H
#define _CMUSICPLAYER_H

class CMusicPlayer
{
	private:
		Mix_Music **songs;
		int songs_count;
		int current_song;

	public:
		CMusicPlayer(void);

		void Init(void);
		void Play(void);
		void Pause(void);
};


#endif
