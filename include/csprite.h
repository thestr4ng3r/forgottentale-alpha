#ifndef _CSPRITE_H
#define _CSPRITE_H

void DrawPixel(SDL_Surface *screen, int x, int y, Uint8 R, Uint8 G, Uint8 B);
Uint32 GetPixel(SDL_Surface *surface, int x, int y);
SDL_Color GetPixelColor(SDL_Surface *surface, int x, int y);
GLuint CreateGLImage(SDL_Surface *img);
GLuint CreateSquareGLImage(SDL_Surface *img);
void ConvertScreenToGL(SDL_Surface *screen, GLuint &gl_screen);
void PutImageToGL(GLuint image, int x, int y, int width, int height, float alpha = 1.0);

void Set2DMatrix(void);
void Blend(void);

class CSprite
{
	public:
		CSprite(int x = 0, int y = 0);
		CSprite(const char *file, int x = 0, int y = 0);
		~CSprite(void);

		bool LoadImage(const char *img);
		void SetImage(GLuint img);
		void LockDestroying(void) { locked = 1; };
		void UnlockDestroying(void) { locked = 0; };
		void DestroyGLImage(void);
		void Destroy(void);

		void PutToGL(int scale_width = 0, int scale_height = 0);

		void SetPosition(int x, int y) { this->x = x; this->y = y; }
		void SetAlpha(float a)	{ alpha = a; }
		void Move(int x, int y)		{ this->x += x; this->y += y; }
		void SetX(int v)		{ x = v; }
		void SetY(int v)		{ y = v; }
		void SetSize(int w, int h) { width = w; height = h; }
		int X(void)			{ return x; }
		int Y(void)			{ return y; }
		int Width(void)			{ return width; }
		int Height(void)		{ return height; }

		int PointCollision(int _x, int _y) { if(_x > x && _y > y && _x < x + width && _y < y + height) return 1; return 0; };

	private:
		int locked;
		int x, y;
		int width, height;
		float alpha;
		GLuint gl_image;
};

#endif
