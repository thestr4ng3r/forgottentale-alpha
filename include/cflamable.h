#ifndef _CFLAMABLE_H
#define _CFLAMABLE_H

struct CFlamable
{
	CGame *game;
	
	int fire_source;

	void SelectFireSource(void);
};

#endif
