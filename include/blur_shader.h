#ifndef _BLUR_SHADER_H
#define _BLUR_SHADER_H

#define BLUR_HORIZONTAL  0
#define BLUR_VERTICAL    1

class CBlurShader : public CShader
{
	private:
		GLint texture_uniform;
		GLint width_uniform;
		GLint height_uniform;
		GLint blur_size_uniform;
		GLint direction_uniform;

	public:
		void Init(void);
		void SetTexture(GLuint tex, int width, int height);
		void SetTexCoord(CVector2 v);
		void SetBlur(float size, int dir);

		void ResetUniforms(void) {};
};

extern CBlurShader blur_shader;

#endif
