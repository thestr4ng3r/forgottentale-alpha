#ifndef _CMAP_H
#define _CMAP_H

class CMap;
class CGame;

// Distances between points are estimated like this: linear distance * ESTIMATE_DIST_FACTOR

#define ESTIMATE_DIST_FACTOR 1.3

inline float EstimateDist(CVector2 start, CVector2 end)
{
	return (end - start).Len() * ESTIMATE_DIST_FACTOR;
}

enum FIELD_NODE_STATE { UNKNOWN, OPEN, CLOSED };

#define FIELD_NODE_NEXT_COUNT 8

struct CFieldNode
{
	CVector pos;
	CVector normal;

	CFieldNode *previous;
	float start_dist;
	float end_dist; // approximated

	union
	{
		struct
		{
			CFieldNode *next_px; // positive x
			CFieldNode *next_nx; // negative x
			CFieldNode *next_py; // positive y
			CFieldNode *next_ny; // negative y

			CFieldNode *next_px_py; // +x, +y
			CFieldNode *next_px_ny; // +x, -y
			CFieldNode *next_nx_py; // -x, +y
			CFieldNode *next_nx_ny; // -x, -y
		};
		CFieldNode *next[FIELD_NODE_NEXT_COUNT];
	};

	FIELD_NODE_STATE state;

	CFieldNode(CVector pos, CVector normal)
	{
		this->pos = pos;
		this->normal = normal;
		start_dist = 0.0;
		end_dist = 0.0;
		previous = 0;
		next_px = next_nx = next_py = next_ny = 0;
	}
};

bool operator<(CFieldNode &a, CFieldNode &b);
bool CompareFieldNodePointers(CFieldNode *a, CFieldNode *b);


struct CWayPoint
{
	CVector v;
	CVector n;
};


struct CWay
{
	list<CWayPoint *> way_points;

	float GetLength(void);
	CWayPoint *WayPointByIndex(int index);
	CVector PositionByWayDist(float way, CVector &dir, float smooth = 0.0); // Returns the position
};

class CMap
{
	private:
		CGame *game;
		CMesh *mesh;
		CMapFile map_file;

		GLuint environment_tex;

		void LoadWay(void);

	public:
		map<int, map<int, CFieldNode *> *> field_nodes;
		static const float field_resolution = 0.5;
		CVector2 start, end;

		CWay *way;
		CWay *way_temp;

		int ground_count;            // ground = basic ground described by triangles
		CSimpleTriangle *ground_t;
		CVector2 ground_min, ground_max, ground_size;

		int water_count;
		CSimpleTriangle *water_t;

		float minx, miny, maxx, maxy;
		
		CMap(CGame *game);
		~CMap(void);

		int LoadFromFile(const char *file);
		int LoadFromFile_old(const char *file);
		int LoadWayFromMapFileHandle(int h);
		int LoadFieldFromMapFileHandle(int h);
		int LoadGroundFromMapFileHandle(int h);
		int LoadWaterFromMapFileHandle(int h);
		int LoadMapRangeFromMapFileHandle(int h);
		int LoadMeshFromFile(const char *file);
		int LoadWayFromFile(const char *file);
		void PutToGL(void);
		void PutEnvironmentToGL(void);
		void PutGroundToGL(int name = 0);
		void PutWaterToGL(void);

		CVector ProjectOnGround(CVector2 v, bool *inside = 0, CVector *normal = 0); // inside is set to false if the coordinates are not on the ground
		void CreateFieldNodes(void);
		CFieldNode *GetFieldNode(int x, int y);
		CFieldNode *GetNearestNode(CVector2 v);
		bool GetDirectWayPossible(CVector a, CVector b, float res = field_resolution);
		CWay *GetWay(void) { return way; }
		CWay *FindShortestWay(bool set, bool temp, CVector2 start);
		CWay *FindShortestWay(bool set = true, bool temp = false) { return FindShortestWay(set, temp, start); }
};

#endif
