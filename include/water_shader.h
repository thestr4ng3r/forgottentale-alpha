#ifndef _WATER_SHADER_H
#define _WATER_SHADER_H

class CWaterShader : public CShader
{
	private:
		GLint tex_uniform;
		GLint normal_uniform;

		GLint color_tex_uniform;
		GLint depth_tex_uniform;

		GLint anim_uniform;

	public:
		void Init(void);
		void SetTexture(GLuint tex, GLuint normal, GLuint color, GLuint depth);
		void SetAnim(float a);
};

extern CWaterShader water_shader;

#endif

