
#ifndef _CPROGRAM_H
#define _CPROGRAM_H

class CGame;

#define PROGRAM_MODE_GAME 1
#define PROGRAM_MODE_INTRO 2

class CProgramIntro;

class CProgram
{
	private:
		CGame *game;
		CProgramIntro *intro;
		int mode;

	public:
		float last_tick;
		float fps;
		
		static void InitGlobal(void);

		CProgram(void);
		~CProgram(void);
		
		void Init(void);
		void Run(void);
		void Paint(void);

		void ResetMode(void);
		void ChangeToGame(void);
		void ChangeToIntro(void);

		void OnEvent(SDL_Event event);
};


GLuint CreateBlendSpriteTexture(void);

class CProgramIntro
{
	private:
		float time;
		CProgram *program;

	public:
		static const float length = 7.0;
		static const float fade_length = 0.5;

		CProgramIntro(CProgram *program);

		void Init(void);
		void Run(void);
		void Paint(void);
};

#endif
