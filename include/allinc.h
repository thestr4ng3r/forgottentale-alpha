#ifndef _ALLINC_H
#define _ALLINC_H

//#define max(a, b) (a > b ? a : b)
//#define min(a, b) (a < b ? a : b)

#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <list>
using namespace std;
#include <time.h>

#define GL_GLEXT_PROTOTYPES

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_image.h>

#include "towerengine.h"
#include "ftlib.h"

#include "OGLFT.h"

#include "resources.h"

#include "particle.h"

#include "defines.h"
#include "csprite.h"
#include "cmusicplayer.h"
#include "canimsprite.h"
#include "water_shader.h"
#include "display_shader.h"
#include "environment_shader.h"
#include "field_shader.h"
#include "blur_shader.h"
#include "cprogram.h"
#include "cgame.h"
#include "cgamehud.h"
#include "cpausemenu.h"
#include "cmap.h"
#include "cflamable.h"
#include "cenemy.h"
#include "ctower.h"
#include "cshot.h"
#include "globals.h"

#endif
