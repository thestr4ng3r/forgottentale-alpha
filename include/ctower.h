#ifndef _CTOWER_H
#define _CTOWER_H

#define TOWER_TYPE_ARROW 0
#define TOWER_TYPE_SLOW 1
#define TOWER_TYPE_ICE 2
#define TOWER_TYPE_FIRE 3
#define TOWER_TYPE_STONE 4

struct CArrowTower;
struct CSlowTower;
struct CIceTower;
struct CFireTower;
struct CStoneTower;

struct CTower : public CFlamable
{
	//CGame *game;
	CTower *chain_next;

	CMeshHandler *mesh;
	
	int type;
	CVector pos;
	float rot;
	CVector shoot_pos;
	float range;
	float power;
	float build_radius;

	float time_r;
	float shoot_time;

	int enemy_crit;

	int name;
	int valid_pos;

	float color_fade_time;
	bool color_fade_active;
	CVector fade_color;
	float color_fade_length;
	float color_fade_max_begin, color_fade_max_end;

	CTower(CGame *game, int t);
	~CTower(void);

	static CTower *CreateTower(CGame *game, int t);

	void ColorFade(CVector color, float length, float max_begin, float max_end);
	void ColorFade(CVector color, float length) { ColorFade(color, length, length * 0.1, length * 0.15); }
	virtual void Run(float time);
	void PutToGL(void);
	void PutRangeToGL(void);
};

struct CArrowTower : public CTower
{
	CArrowTower(CGame *game) : CTower(game, TOWER_TYPE_ARROW) { };

	void Run(float time);
};

struct CSlowTower : public CTower
{
	CSlowTower(CGame *game) : CTower(game, TOWER_TYPE_SLOW) { };

	void Run(float time);
};

struct CIceTower : public CTower
{
	CIceTower(CGame *game) : CTower(game, TOWER_TYPE_ICE) { };

	void Run(float time);
};

struct CFireTower : public CTower
{
	CFireTower(CGame *game) : CTower(game, TOWER_TYPE_FIRE) { SelectFireSource(); };
	~CFireTower(void);

	void Run(float time);
};

struct CStoneTower : public CTower
{
	CStoneTower(CGame *game) : CTower(game, TOWER_TYPE_STONE) { };
	~CStoneTower(void);

	void Run(float time);
};

#endif
