#ifndef _CPAUSEMENU_H
#define _CPAUSEMENU_H

#define PAUSE_MENU_FADE_IN		0
#define PAUSE_MENU_FADE_OUT		1
#define PAUSE_MENU_VISIBLE		2
#define PAUSE_MENU_INVISIBLE	3

#define PAUSE_MENU_BLUR_SIZE	1.0

class CPauseMenu
{
private:
	CGame *game;

	float fade;
	int mode;
	CSprite sprite;

public:
	CPauseMenu(CGame *game, const char *file);

	float GetFade(void) 	{ return fade; };
	int GetMode(void) 		{ return mode; };
	void MakeVisible(void)	{ mode = PAUSE_MENU_FADE_IN; };
	void Run(float time);
	void PutToGL(void);
	void OnEvent(SDL_Event event);
};

#endif
