#ifndef _DEFINES_H
#define _DEFINES_H


#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)


#define MIN_FRAME_TIME 15

#define PRESET_FULLSCREEN 0
#define PRESET_DISPLAY_WIDTH 640
#define PRESET_DISPLAY_HEIGHT 480
#define PRESET_ANTI_ALIASING_LEVEL 2

#define ATTR_NORMAL_LOC        9
#define ATTR_TANG_X_LOC       10
#define ATTR_TANG_Y_LOC       11

#define HEADER "Forgotten Tale Alpha 2 Settings File...\n"
#define FILE_READ_NOT_EXISTING -1
#define FILE_READ_UNEXPECTED_EOF -2
#define FILE_READ_UNEXPECTED_NEW_LINE -3
#define FILE_READ_WRONG_HEADER -4
#define FILE_READ_NAME_NOT_AVAILABLE -5

// Game Modes
#define GAME_MODE_NONE 0
#define GAME_MODE_BUILD_TOWER 1
#define GAME_MODE_SELECTED_TOWER 2
#define GAME_MODE_PAUSE 3

// Enemy Criterion
#define ENEMY_CRIT_FIRST 0
#define ENEMY_CRIT_LAST 1
#define ENEMY_CRIT_WEAKEST 2
#define ENEMY_CRIT_STRONGEST 3

#define FONT_COUNT 4

#define SQRT_2 1.414213562

#endif
