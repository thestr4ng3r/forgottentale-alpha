#ifndef _CGAMEMENU_H
#define _CGAMEMENU_H

struct CGameHUD
{
	CGame *game;
	CProgram *program;

	CSprite background;
	CSprite arrow_tower_button;
	CSprite slow_tower_button;
	CSprite ice_tower_button;
	CSprite fire_tower_button;
	CSprite stone_tower_button;
	CSprite enemy_button;
	CSprite life_display;
	CSprite pointer;

	CGameHUD(CGame *game);

	void Load(void);
	void Refresh(void);
	void PutToGL(void);

	void OnMouseDown(int x, int y);
};

#endif
