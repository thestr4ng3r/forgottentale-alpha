#ifndef _PARTICLE_H
#define _PARTICLE_H

struct CParticle
{
	CVector pos;
	CVector velocity;
	float r, g, b, a;
	float age;
	GLuint tex;

	CVector vel_rand_v;
	float vel_rand_r;
	float vel_rand_max;
	CVector pos_rand_v;
	float pos_rand_r;
	float pos_rand_max;
	float life_time;

	float sq_dist;

	void Set(CVector _pos, CVector _velocity, float _r, float _g, float _b, float _a, float _age, GLuint _tex)
			{ pos = _pos; velocity = _velocity; r = _r; g = _g; b = _b; a = _a, age = _age; tex = _tex; };
};

struct CParticleSource
{
	CVector pos;
	float count;
	float radius;
	float time;
	CVector velocity;
	float vel_rand;
};

class CParticleContext
{
	protected:
		list<CParticle *> p;
		//vector<CParticleSource *> src;
		CParticleSource *src;
		int src_count;
		int max;

		struct particle_template // template for new particles
		{
			float r, g, b, a;

			float life_time_min, life_time_max;

			GLuint *tex;
			int tex_max;
		} t;

		struct range
		{
			CVector a;
			CVector b;
		} range;

		CVector gravity;

	public:
		CParticleContext(int src_c);
		~CParticleContext();

		void Velocity(int i, CVector v, float r = 0.0);
		void Color(float r, float g, float b, float a);
		CParticle *Add(CVector src, float radius = 0.0, CVector velocity = Vec(0.0, 0.0, 0.0), float vel_rand = 0.0);
		void Source(int i, float c, CVector src, float radius = 0.0); // c = particle/second
		void ClearSources(void);
		void ClearSource(int i);
		void Textures(GLuint *tex, int max);
		//void Remove(int i);
		void LifeTime(float a, float b);
		void LifeTime(float a) { LifeTime(a, a); };
		void Range(CVector a, CVector b);
		void Gravity(CVector g);
		void Move(float time);
		void Sort(CVector cam_pos, int inv = 0);
		int Count(void) { return p.size(); };
		list<CParticle *> *GetList(void) { return &p; }
		//CParticle *Particle(int i) { if(i>=(int)p.size()) return 0; return *(p.begin() + i); };


		//void Print(void) { for(vector<CParticle *>::iterator i=p.begin(); i!=p.end(); i++)
		//					{ printf("particle at address %ld: ", (long)(*i)); printf("%f, %f, %f\n", pvec(((*i)->pos))); }}
};

void _glPaintSprite(CVector p, float size, CVector cam);

#endif
