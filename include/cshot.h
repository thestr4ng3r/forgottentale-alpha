#ifndef _CSHOT_H
#define _CSHOT_H

struct CShot
{
	CGame *game;
	CShot *chain_next;

	CEnemy *aim;
	int type;

	CMeshHandler *mesh;
	
	CVector pos;
	CVector dir;
	CVector top;
	float speed;
	float stunned_time;
	float slow_time;
	float burn_time;
	float damage;

	int to_delete;
	int hit;

	CShot(CGame *game, CEnemy *aim, CTower *tower);
	~CShot(void);

	void Run(float time);
	void PutToGL(void);
};

#endif
