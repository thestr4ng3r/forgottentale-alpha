#ifndef _CGAME_H
#define _CGAME_H

#define FIRE_TEXTURES 4

class CMap;
struct CWayPoint;
struct CEnemy;
struct CTower;
struct CShot;

struct CGameHUD;
class CPauseMenu;

class CGame
{
	private:
		int debug_mode;
		int mouse_buttons[3];
		CSprite debug_sprite;

		CAnimSprite loading_anim;
		Uint32 loading_last_redraw;
		Uint32 loading_tick;
		bool loading;

		struct debug_camera
		{
			float speed;
			float rot_speed;
			CVector pos;
			CVector focus;
			CVector2 rot;

			float angle;
		} debug_camera;

		struct camera
		{
			CVector pos;
			CVector focus;
			CVector focus_horz;
			CVector focus_vert;
			float dist;
			CVector2 angle;
			
			float min_dist, max_dist;
			float min_angle, max_angle;

			CVector move_focus;
			float move_dist; 
			CVector2 move_angle;

			float focus_speed;
			float dist_speed;
			CVector2 angle_speed;

			GLint viewport[4];
		} camera;

		CVector current_cam_pos, current_focus;

		struct light
		{
			CVector pos;
			CVector color;
		} light;
		
		struct shadow
		{
			GLuint tex;
			GLuint fbo;
			GLuint rbo;

			static const int width = 4096;
			static const int height = 4096;
		} shadow;

		struct water
		{
			GLuint tex;
			GLuint fbo;
			GLuint rbo;

			GLuint normal_tex;

			int width;
			int height;

			float anim;
		} water;

		struct clip
		{
			CVector v;
			float d;
		} clip;

		struct render
		{
			GLuint color_tex;
			GLuint color_tex_dump;
			GLuint color_tex_temp;
			GLuint color_tex_blurred;
			GLuint depth_tex;
			GLuint depth_tex_dump;
			GLuint fbo;

			float aa_level;
		} render;

		struct blur
		{
			GLuint fbo;
		} blur;

		void BlurTexture(GLuint in, GLuint temp, GLuint out, int width, int height, float size);

		CSprite mouse_cursor;

		CMap *map;

		struct field
		{
			GLuint color_tex;
			GLuint depth_tex;
			GLuint fbo;

			unsigned char *data;

			static const unsigned int size = 64;
			CVector2 scale;
			CVector max, min;

			bool show;

			CVector2 GetTexCoord(CVector v);
		} field;

		struct matrices
		{
			float model_view[16];
			float projection[16];
			GLdouble model_view_d[16];
			GLdouble projection_d[16];
			GLint view_port[4];
			static const float near_clip = 0.1;
			static const float far_clip = 200.0;
			float both[16];
			float inverse[16];
		} matrices;


		struct defeat
		{
			float anim;

			int defeat;
		} defeat;


		CSprite defeat_sprite;

		GLuint particle_tex;

		CVector debug_point;

		CGameHUD *game_hud;
		CPauseMenu *pause_menu;

		int mouse_locked;

		void OnMouseDown(int x, int y);
		void OnKeyDown(SDL_KeyboardEvent event);
		void OnKeyUp(SDL_KeyboardEvent event);
		void OnMouseMotion(SDL_MouseMotionEvent event);
		void OnMouseButtonDown(SDL_MouseButtonEvent event);
		void OnMouseButtonUp(SDL_MouseButtonEvent event);
		int PickMouseV(int x, int y, CVector *v);
		int PickFreeBuildPlace(int x, int y, CTower *t, CVector *v, int *valid_pos = 0);
		void CancelBuild(void);
		void CalculateDebugCamera(void);
		void CalculateCamera(void);

		void CalculateParticles(float time);

		void PaintLifeDisplay(CVector p, float l, float alpha = 1.0);

	public:
		struct templates
		{
			struct towers
			{
				struct arrow
				{
					CMesh tower;
					CMesh shot;
					static const int price = 10;
				} arrow;
				struct slow
				{
					CMesh tower;
					CMesh shot;
					static const int price = 20;
				} slow;
				struct ice
				{
					CMesh tower;
					CMesh shot;
					static const int price = 30;
				} ice;
				struct fire
				{
					CMesh tower;
					CMesh shot;
					static const int price = 30;
				} fire;
				struct stone
				{
					CMesh tower;
					static const int price = 5;
				} stone;
			} towers;

			struct textures
			{
				struct particles
				{
					GLuint fire[FIRE_TEXTURES];
					GLuint smoke;
				} particles;
			} textures;

			CMesh enemy;
		} templates;

		struct fire
		{
			static const int max_src = 100;
			float speed;
			CParticleContext *context;
			int *free_src;
		} fire;


		int show_fps;


		CProgram *program;
		
		// TODO: Enemies, Tower und Shots in std::list o.ä. speichern
		CEnemy *first_enemy;
		CTower *first_tower;
		CShot *first_shot;

		CTower *selected_tower;
		int game_mode;

		int lifes;
		int mana;

		static void InitGlobalGame(void);

		CGame(CProgram *program);
		~CGame(void);

		void Init(void);
		void InitSystem(void);
		void InitGame(void);
		void QuitGame(void);
		void Destroy(void);
		void ProcessMouse(void);
		void Run(void);
		float Paint(void);
		void PaintLoading(void);
		void RenderShadows(void);
		void RenderWater(void);
		void RenderField(void);
		void PaintGL(int width, int height);
		void PaintParticles(void);
		void DumpGLBuffer(GLuint tex, int width, int height);
		void Paint2D(int width, int height);
		void RenderScene(void);

		unsigned char GetFieldNodeValue(CVector v);

		CVector Project(CVector v);

		void SetShadowTextureMatrix(void);
		void SetRenderTextureMatrix(void);
		
		void OnEvent(SDL_Event event);

		int GetDebugMode(void) { return debug_mode; };
		CVector GetCamPos(void) { return debug_mode ? debug_camera.pos : camera.pos; };
		CVector GetFocus(void) { return debug_mode ? debug_camera.focus : camera.focus; };
		CMap *GetMap(void) { return map; };


		void PickTower(int x, int y);
		void InitBuildTower(int t);
		bool CompleteBuildTowerCheck(void);
		void CompleteBuildTower(void);
		int TowerPrice(int t); 
		void CreateEnemy(void);
};


#endif
