#ifndef _CANIMSPRITE_H
#define _CANIMSPRITE_H

class CAnimSprite
{
	public:
		CAnimSprite(int x = 0, int y = 0);
		~CAnimSprite(void);

		bool LoadAnimation(int c, const char **img, float fps = 30.0);
		void SetImage(GLuint img);

		void PutToGL(int scale_width = 0, int scale_height = 0);

		void SetPosition(int x, int y) 	{ this->x = x; this->y = y; };
		void SetAlpha(float a)			{ alpha = a; };
		void Move(int x, int y)			{ this->x += x; this->y += y; };
		void SetX(int v)				{ x = v; };
		void SetY(int v)				{ y = v; };
		int X(void)						{ return x; };
		int Y(void)						{ return y; };
		int Width(void)					{ return width; };
		int Height(void)				{ return height; };

		void SetTime(float t)			{ time = fmod(t, (float)frame_count / fps); }
		void Play(float t)				{ SetTime(time + t); }

		int PointCollision(int _x, int _y) { if(_x > x && _y > y && _x < x + width && _y < y + height) return 1; return 0; };

	private:
		int x, y;
		int width, height;
		float alpha;

		float fps;
		float time;
		int frame_count;
		GLuint *frames;
};

#endif
