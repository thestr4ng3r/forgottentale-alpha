#ifndef _FIELD_SHADER_H
#define _FIELD_SHADER_H

class CFieldShader : public CShader
{
	private:
		GLint towers_uniform;
		GLint towers_range_uniform;
		GLint towers_count_uniform;

	public:
		void Init(void);
		void SetTowers(int count, GLfloat *data, GLfloat *range);

		void ResetUniforms(void) {};
};

extern CFieldShader field_shader;

#endif
