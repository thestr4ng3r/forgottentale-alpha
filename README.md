This is the Alpha Repository for Forgotten Tale.

This is not the actual git repo I am using to develop the game (that one is private). I only update it when I officially release a new version. Take a look at the commits for a version history.

Pre-built packages are available as Downloads here!
