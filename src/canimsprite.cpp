#include "allinc.h"

CAnimSprite::CAnimSprite(int x, int y)
{
	this->x = x;
	this->y = y;
	width = height = 0;

	alpha = 1.0;

	frames = 0;
	frame_count = 0;

	time = 0.0;
	fps = 0.0;
}

CAnimSprite::~CAnimSprite()
{
	glDeleteTextures(frame_count, frames);
}

bool CAnimSprite::LoadAnimation(int c, const char **img, float fps)
{
	int i;
	bool success = true;

	frame_count = c;
	frames = new GLuint[c];

	for(i=0; i<c; i++)
	{
		frames[i] = LoadGLTexture(img[i], &width, &height);
		if(frames[i] == 0)
			success = false;
	}

	this->fps = fps;

	return success;
}


void CAnimSprite::PutToGL(int scale_width, int scale_height)
{
	int scale_width2 = scale_width != 0 ? scale_width : width;
	int scale_height2 = scale_height != 0 ? scale_height : height;

	int current_frame = (int)(fps * time);

	PutImageToGL(frames[current_frame], x, y, scale_width2, scale_height2, alpha);
}

