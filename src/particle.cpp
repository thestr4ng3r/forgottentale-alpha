#include "allinc.h"

CParticleContext::CParticleContext(int src_c)
{
	this->max = max;
	Range(Vec((float)-INFINITY, (float)-INFINITY, (float)-INFINITY), Vec((float)INFINITY, (float)INFINITY, (float)INFINITY));
	Color(1.0, 1.0, 1.0, 1.0);
	Gravity(Vec(0.0, 0.0, 0.0));
	Textures(0, 0);
	src = new CParticleSource[src_c];
	src_count = src_c;
	ClearSources();
	LifeTime(0.0);
}

CParticleContext::~CParticleContext()
{
}

void CParticleContext::Velocity(int i, CVector v, float r)
{
	src[i].velocity = v;
	src[i].vel_rand = r;
}

void CParticleContext::Color(float r, float g, float b, float a)
{
	t.r = r; t.g = g; t.b = b; t.a = a;
}

CParticle *CParticleContext::Add(CVector src, float radius, CVector velocity, float vel_rand)
{
	GLuint tex;
	CVector pos;
	CVector vel;
	float r;
	CParticle *n;

	n = new CParticle();

	if(t.tex)
		tex = t.tex[(int)(((double)rand() / (double)RAND_MAX) * (double)t.tex_max)];
	else 
		tex = 0;

	pos = src;

	if(radius != 0.0)
	{
		r = ((float)rand() / (float)RAND_MAX);
		r *= r;
		r *= 10.0;
		r = min(r, 1.0);
		n->pos_rand_r = radius * r;
		n->pos_rand_v = RandVec();
		pos += n->pos_rand_r * n->pos_rand_v;
	}

	vel = velocity;

	if(vel_rand != 0.0)
	{
		n->vel_rand_r = vel_rand * ((float)rand() / (float)RAND_MAX);
		n->vel_rand_v = RandVec();
		vel += n->vel_rand_r * n->vel_rand_v;
	}

	n->pos_rand_max = radius;
	n->vel_rand_max = vel_rand;
	n->life_time = t.life_time_min + ((t.life_time_max - t.life_time_min) * ((float)rand() / (float)RAND_MAX));

	n->Set(pos, vel, t.r, t.g, t.b, t.a, 0.0, tex);

	p.push_back(n);

	return n;
}

/*void CParticleContext::Remove(int i)
{
	if(i<0 || i>=(int)p.size())
		return;

	p.erase(p.begin() + i);
}*/

void CParticleContext::ClearSources(void)
{
	int i;
	
	for(i=0; i<src_count; i++)
		ClearSource(i);
}

void CParticleContext::ClearSource(int i)
{
	Source(i, 0.0, Vec(0.0, 0.0, 0.0), 0.0);
	Velocity(i, Vec(0.0, 0.0, 0.0));
	src[i].time = 0.0;
}

void CParticleContext::Source(int i, float c, CVector pos, float radius)
{
	src[i].pos = pos;
	src[i].count = c;
	src[i].radius = radius;
}

void CParticleContext::Range(CVector a, CVector b)
{
	range.a = vecmin(a, b);
	range.b = vecmax(a, b);
}

void CParticleContext::Gravity(CVector g)
{
	gravity = g;
}

void CParticleContext::LifeTime(float a, float b)
{
	t.life_time_min = a;
	t.life_time_max = b;
}

void CParticleContext::Textures(GLuint *tex, int max)
{
	if(!tex)
	{
		t.tex = 0;
		t.tex_max = 0;
	}
	else
	{
		t.tex = tex;
		t.tex_max = max;
	}
}

void CParticleContext::Move(float time)
{
	int j;
	float w;
	list<CParticle *>::iterator i;
	list<CParticle *>::iterator k;


	for(j=0; j<src_count; j++)
	{
		if(src[j].count <= 0.0)
			continue;
		src[j].time += time;
		w = 1.0 / (float)src[j].count;
		while(src[j].time > w)
		{
			Add(src[j].pos, src[j].radius, src[j].velocity, src[j].vel_rand);
			src[j].time -= w;
		}
	}

	for(i=p.begin(); i!=p.end();)
	{
		(*i)->pos += (*i)->velocity * time;
		(*i)->velocity += gravity * time;
		(*i)->age += time;
		if(!PointInRange((*i)->pos, range.a, range.b))
		{
			k = i;
			i++;
			p.erase(k);
		}
		else
			i++;
	}
}

void _glPaintSprite(CVector p, float size, CVector cam)
{
	CVector x, y, z;

	z = cam - p;
	y = Vec(0.0, 1.0, 0.0);
	x = Cross(z, y);
	y = Cross(z, x);

	x.Normalize();
	y.Normalize();
	z.Normalize();

	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3fv((-x * size + y * size + p).v);
	glTexCoord2f(0.0, 1.0);
	glVertex3fv((-x * size + -y * size + p).v);
	glTexCoord2f(1.0, 1.0);
	glVertex3fv((x * size + -y * size + p).v);
	glTexCoord2f(1.0, 0.0);
	glVertex3fv((x * size + y * size + p).v);
	glEnd();
}

bool Compare(CParticle *a, CParticle *b)
{
	return a->sq_dist > b->sq_dist;
}

void CParticleContext::Sort(CVector cam_pos, int inv)
{
	list<CParticle *>::iterator i;

	//printf("sorting %d particles!\n", (int)p.size());
	for(i=p.begin(); i!=p.end(); i++)
	{
		//printf("calculating dist for %ld: ", (long)(*i));
		//printf("%f, %f, %f\n", pvec((*i)->pos));
		(*i)->sq_dist = ((*i)->pos - cam_pos).SquaredLen();
	}

	p.sort(Compare);
	if(inv)
		reverse(p.begin(), p.end());
}


/*void CParticleContext::Sort(CVector cam_pos, int inv) // Bubblesort
{
	int f; // finished
	float m = inv ? -1.0 : 1.0; // multiplicator
	float d1, d2;
	CParticle *t;
	int i;

	if(count <= 1)
		return;

	do
	{
		f = 1;
		for(i=0; i<count-1; i++)
		{
			d1 = m * (p[i]->pos - cam_pos).SquaredLen();
			d2 = m * (p[i + 1]->pos - cam_pos).SquaredLen();

			if(d1 < d2)
			{
				f = 0;
				t = p[i];
				p[i] = p[i + 1];
				p[i + 1] = t;
			}
		}
	}
	while(!f);
}*/

/*void CParticleContext::Sort(CVector cam_pos, int inv) 
{
	CParticle **o;
	float *v;
	CVector d;
	int i, j, k;
	int skip;
	float max_dist;
	int max_p;

	o = p;
	p = new CParticle *[max];
	v = new float[max];

	for(i=0; i<count; i++)
	{
		d = cam_pos - o[i]->pos;
		v[i] = d.SquaredLen();
		if(inv)
			v[i] *= -1.0;
	}


	for(i=0; i<count; i++)
	{
		max_dist = -INFINITY;
		max_p = 0;

		for(j=0; j<count; j++)
		{
			if(v[j] >= max_dist)
			{
				skip = 0;
				for(k=0; k<i; k++)
				{
					if(p[k] == o[j])
					{
						skip = 1;
						break;
					}
				}
				if(skip)
					continue;

				max_dist = v[j];
				max_p = j;
			}
		}
		p[i] = o[max_p];
	}

	delete [] o;
}*/
