#include "allinc.h"

#include "cmap.h"


int CMap::LoadFromFile(const char *file)
{
	static const char *e_fn[6] = {	"./maps/dev/env/xn.png",
			"./maps/dev/env/xp.png",
			"./maps/dev/env/yp.png",
    		"./maps/dev/env/yn.png",
    		"./maps/dev/env/zp.png",
    		"./maps/dev/env/zn.png" };
	char *mesh_file, *path;

	if(map_file.LoadFromFile(file) != 0)
		return 0;

	game->PaintLoading();

	ground_count = map_file.ground.size();
	ground_t = map_file.ground.data();
	ground_min = map_file.ground_min;
	ground_max = map_file.ground_max;
	ground_size = ground_max - ground_min;

	game->PaintLoading();

	start = map_file.start;
	end = map_file.end;
	way = 0;
	way_temp = 0;
	game->PaintLoading();

	CreateFieldNodes();
	game->PaintLoading();

	water_count = map_file.water.size();
	water_t = map_file.water.data();
	game->PaintLoading();

	minx = -12.0;
	miny = -12.0;
	maxx = 12.0;
	maxy = 12.0;

	game->PaintLoading();
	environment_tex = LoadGLCubeMap(e_fn);
	game->PaintLoading();

	path = PathOfFile(file);
	mesh_file = new char[strlen(path) + 1 + strlen(map_file.mesh_file) + 1];
	snprintf(mesh_file, strlen(path) + 1 + strlen(map_file.mesh_file) + 1, "%s/%s", path, map_file.mesh_file);
	game->PaintLoading();

	mesh = new CMesh();
	int r = mesh->LoadFromFile(mesh_file);
	game->PaintLoading();
	return r;
}

CVector CMap::ProjectOnGround(CVector2 v, bool *inside, CVector *normal)
{
	int gi;
	CVector r = Vec(v.x, 0.0, v.y);
	CVector n;

	if(inside)
		*inside = false;

	for(gi=0; gi<ground_count; gi++)
	{
		if(!VecOverTriangle(v, ground_t[gi].Top()))
			continue;
		n = ground_t[gi].Normal();
		if(normal)
			*normal = n;
		r.SetFromPlane(v, n, Dot(n, ground_t[gi].v[0]));
		if(inside)
			*inside = true;
		break;
	}
	return r;
}

bool operator<(CFieldNode &a, CFieldNode &b)
{
	return a.start_dist + a.end_dist < b.start_dist + b.end_dist;
}

bool CompareFieldNodePointers(CFieldNode *a, CFieldNode *b)
{
	return *a < *b;
}

CFieldNode *CMap::GetFieldNode(int x, int y)
{
	map<int, map<int, CFieldNode *> *>::iterator i;
	map<int, CFieldNode *>::iterator j;

	if((i = field_nodes.find(x)) != field_nodes.end())
	{
		j = i->second->find(y);
		if(j != i->second->end())
			return j->second;
	}

	return 0;
}

void CMap::CreateFieldNodes(void)
{
	int x, y;
	int max_count_x, max_count_y;
	CVector2 current_pos;
	CVector pos;
	CVector normal;
	bool inside;
	map<int, CFieldNode *> *vertical_nodes;
	CFieldNode *node;

	field_nodes.clear(); // TODO: Nodes wirklich deleten, nicht nur die map clearen

	max_count_x = (int)(ground_size.x / field_resolution);
	max_count_y = (int)(ground_size.y / field_resolution);

	for(x=0; x<max_count_x; x++)
	{
		vertical_nodes = new map<int, CFieldNode *>();
		field_nodes.insert(pair<int, map<int, CFieldNode *> *>(x, vertical_nodes));
		current_pos.x = ground_min.x + ((float)x) * field_resolution;
		for(y=0; y<max_count_y; y++)
		{
			current_pos.y = ground_min.y + ((float)y) * field_resolution;
			pos = ProjectOnGround(current_pos, &inside, &normal);
			/*inside = false;
			for(int gi=0; gi<ground_count; gi++)
			{
				if(!VecOverTriangle(current_pos, ground_t[gi].Top()))
					continue;
				inside = true;
				break;
			}*/
			if(!inside)
				continue;
			vertical_nodes->insert(pair<int, CFieldNode *>(y, new CFieldNode(pos, normal)));
		}
	}

	for(x=0; x<max_count_x; x++)
	{
		for(y=0; y<max_count_y; y++)
		{
			node = GetFieldNode(x, y);
			if(!node)
				continue;
			node->next_nx = GetFieldNode(x-1, y);
			node->next_px = GetFieldNode(x+1, y);
			node->next_ny = GetFieldNode(x, y-1);
			node->next_py = GetFieldNode(x, y+1);

			node->next_px_py = GetFieldNode(x+1, y+1);
			node->next_px_ny = GetFieldNode(x+1, y-1);
			node->next_nx_py = GetFieldNode(x-1, y+1);
			node->next_nx_ny = GetFieldNode(x-1, y-1);
			node->end_dist = EstimateDist(node->pos.xz(), end);
		}
	}
}

CFieldNode *CMap::GetNearestNode(CVector2 v)
{
	CFieldNode *node;
	CVector2 index;
	map<int, map<int, CFieldNode *> *>::iterator ai;
	map<int, CFieldNode *>::iterator bi;
	float min_dist;
	float dist;

	index = v - ground_min;
	index *= 1.0 / field_resolution;
	index.x = round(index.x); index.y = round(index.y); // finding the nearest node to the start point
	node = GetFieldNode((int)index.x, (int)index.y);

	if(node)
		return node;

	node = 0;
	min_dist = INFINITY;

	for(ai = field_nodes.begin(); ai != field_nodes.end(); ai++)
	{
		for(bi = (*ai).second->begin(); bi != (*ai).second->end(); bi++)
		{
			if((dist = ((*bi).second->pos.xz() - v).SquaredLen()) < min_dist)
			{
				min_dist = dist;
				node = (*bi).second;
			}
		}
	}

	return node;
}

bool CMap::GetDirectWayPossible(CVector a, CVector b, float res)
{
	CVector dir = b - a;
	float max_dist = dir.Len();
	float dist;

	dir.Normalize();

	if(game->GetFieldNodeValue(b) < 0.75 * 255)
				return false;

	for(dist = 0.0; dist <= max_dist; dist += res)
	{
		if(game->GetFieldNodeValue(a + dir * dist) < 0.75 * 255)
			return false;
	}

	return true;
}

CWay *CMap::FindShortestWay(bool set, bool temp, CVector2 start) // Dijkstra Algorithm
{
	int n;

	map<int, map<int, CFieldNode *> *>::iterator ai;
	map<int, CFieldNode *>::iterator bi;

	list<CFieldNode *> open_list;
	list<CFieldNode *> closed_list;

	list<CFieldNode *>::iterator oli;

	CFieldNode *start_node = GetNearestNode(start);
	CFieldNode *end_node = GetNearestNode(end);

	CFieldNode *node;

	CWay *new_way;
	CWayPoint *wp;

	if(!start_node || !end_node)
		return false;

	for(ai = field_nodes.begin(); ai != field_nodes.end(); ai++)
	{
		for(bi = (*ai).second->begin(); bi != (*ai).second->end(); bi++)
		{
			(*bi).second->state = game->GetFieldNodeValue((*bi).second->pos) < 0.75 * 255 ? CLOSED : UNKNOWN;
		}
	}

	open_list.push_front(start_node);
	start_node->state = OPEN;
	start_node->start_dist = 0.0;
	start_node->previous = 0;

	while(true)
	{
		if(open_list.empty())
			return 0;

		oli = min_element(open_list.begin(), open_list.end(), CompareFieldNodePointers);
		node = *oli;
		open_list.erase(oli);

		if(node == end_node)
			break;

		closed_list.push_front(node);
		node->state = CLOSED;

		for(n=0; n<FIELD_NODE_NEXT_COUNT; n++)
		{
			if(!node->next[n])
				continue;

			switch(node->next[n]->state)
			{
			case OPEN:
				if(node->next[n]->start_dist < node->start_dist + (n>=4 ? SQRT_2 : 1.0) * field_resolution)
					break;
			case UNKNOWN:
				node->next[n]->start_dist = node->start_dist + (n>=4 ? SQRT_2 : 1.0) * field_resolution;
				node->next[n]->previous = node;
				if(node->next[n]->state != OPEN)
					open_list.push_front(node->next[n]);
				node->next[n]->state = OPEN;
				break;
			default:
				break;
			}
		}
	}

	new_way = new CWay();

	for(node = end_node; node != 0; node = node->previous)
	{
		if(node != end_node && node->previous != 0)
			if(GetDirectWayPossible(new_way->way_points.front()->v, node->previous->pos))
				continue;

		wp = new CWayPoint();
		wp->n = node->normal;
		wp->v = node->pos;
		new_way->way_points.push_front(wp);
	}

	if(set)
	{
		if(temp)
			way_temp = new_way;
		else
			way = new_way;
	}

	return new_way;
}



int CMap::LoadMeshFromFile(const char *file)
{
	if(!mesh->LoadFromFile(file))
		return 0;

	return 1;
}


void CMap::PutEnvironmentToGL(void)
{
	glDisable(GL_DEPTH_TEST);

	environment_shader.BindShader();
	environment_shader.SetCubeMap(environment_tex);

	CVector cam_pos = game->GetCamPos();
	float es = 100.0;

	glPushMatrix();
	glTranslatef(cam_pos.x, cam_pos.y, cam_pos.z);

	glBegin(GL_QUADS);
		glVertex3f(es, es, es);
		glVertex3f(es, -es, es);
		glVertex3f(-es, -es, es);
		glVertex3f(-es, es, es);
	glEnd();

	glBegin(GL_QUADS);
		glVertex3f(es, es, -es);
		glVertex3f(es, es, es);
		glVertex3f(-es, es, es);
		glVertex3f(-es, es, -es);
	glEnd();

	glBegin(GL_QUADS);
		glVertex3f(-es, es, -es);
		glVertex3f(-es, -es, -es);
		glVertex3f(es, -es, -es);
		glVertex3f(es, es, -es);
	glEnd();

	glBegin(GL_QUADS);
		glVertex3f(es, -es, -es);
		glVertex3f(-es, -es, -es);
		glVertex3f(-es, -es, es);
		glVertex3f(es, -es, es);
	glEnd();

	glBegin(GL_QUADS);
		glVertex3f(-es, es, es);
		glVertex3f(-es, -es, es);
		glVertex3f(-es, -es, -es);
		glVertex3f(-es, es, -es);
	glEnd();

	glBegin(GL_QUADS);
		glVertex3f(es, es, -es);
		glVertex3f(es, -es, -es);
		glVertex3f(es, -es, es);
		glVertex3f(es, es, es);
	glEnd();

	glPopMatrix();

	glEnable(GL_DEPTH_TEST);

}

void CMap::PutToGL(void)
{
	//int i;
	//CWayPoint *p;

	if(!mesh)
		return;

	mesh->PutToGL(game->GetCamPos());
		/*UseNoShader();
	if(game->GetDebugMode())
	{
		glDisable(GL_LIGHTING);
		SetColor(GL_DIFFUSE, 0.0, 0.0, 1.0);
		//glDisable(GL_DEPTH_TEST);
		glLineWidth(4.0);
		glPushMatrix();
		glTranslatef(0.0, 0.01, 0.0);
		glBegin(GL_LINES);
		for(i=0; i<way_point_count; i++)
			if((p = WayPointByIndex(i)) != 0)
			{
				p->v.PutToGL();
				if(i > 0 && i < way_point_count - 1)
					p->v.PutToGL();
			}

		PutGroundToGL();
		glEnd();
		glPopMatrix();
		glLineWidth(1.0);
		
	}*/
}

void CMap::PutGroundToGL(int name)
{
	int i;

	for(i=0; i<ground_count; i++)
	{
		if(name)
			glLoadName(i);
		glBegin(GL_TRIANGLES);
		ground_t[i].v[0].PutToGL();
		ground_t[i].v[1].PutToGL();
		ground_t[i].v[2].PutToGL();
		glEnd();
	}
}

void CMap::PutWaterToGL()
{
	int i;

	for(i=0; i<water_count; i++)
	{
		glBegin(GL_TRIANGLES);
		water_t[i].v[0].PutToGL();
		water_t[i].v[1].PutToGL();
		water_t[i].v[2].PutToGL();
		glEnd();
	}
}


CMap::CMap(CGame *game)
{
	this->game = game;
	way = 0;
	way_temp = 0;
	mesh = 0;
}

CMap::~CMap(void)
{
}





CVector CWay::PositionByWayDist(float way, CVector &dir, float smooth)
{
	int i;
	float w;
	float s;
	float d_w;
	CVector a, b;
	CVector r;
	CVector pos;
	CVector other_dir;
	float mix;

	if(way_points.empty())
		return Vec(0.0, 0.0, 0.0);
	if(way_points.size() == 1)
		return way_points.front()->v;

	if(way < 0.0)
	{
		dir = WayPointByIndex(0)->v - WayPointByIndex(1)->v;
		dir.Normalize();
		dir *= -1.0;
		pos = WayPointByIndex(0)->v + dir * way;
		return pos;
	}

	if(way >= GetLength())
	{
		dir = WayPointByIndex(way_points.size() - 1)->v - WayPointByIndex(way_points.size() - 2)->v;
		dir.Normalize();
		pos = WayPointByIndex(way_points.size() - 1)->v + dir * (way - GetLength());
		return pos;
	}

	w = 0.0;

	for(i=0; i<(int)way_points.size()-1; i++)
	{
		a = WayPointByIndex(i)->v;
		b = WayPointByIndex(i + 1)->v;
		dir = b - a;
		d_w = dir.Len();
		dir.Normalize();
		if(way > w && way <= w + d_w)
		{
			r = a;
			r += dir * (way - w);
			pos = r;
			if(smooth > 0.0)
			{
				s = smooth * 0.5;
				if(d_w < 2.0 * s)
					s = d_w / 2.0;
				if(way - w <= s && i > 0) // mix with last dir
				{
					other_dir = WayPointByIndex(i)->v - WayPointByIndex(i - 1)->v;
					other_dir.Normalize();
					mix = 1.0 - ((way - w) / s);
					dir = Mix(dir, other_dir, mix * 0.5);
					
				}
				else if(way - w >= d_w - s && i < (int)way_points.size() - 2) // mix with next dir
				{
					other_dir = WayPointByIndex(i + 2)->v - WayPointByIndex(i + 1)->v;
					other_dir.Normalize();
					mix = ((way - w) - (d_w - s)) / s;
					dir = Mix(dir, other_dir, mix * 0.5);
				}
			}
			return pos;
		}
		w += d_w;
	}

	return Vec(0.0, 0.0, 0.0);
}




float CWay::GetLength(void)
{
	float len;
	int i;

	len = 0.0;

	for(i=0; i<(int)way_points.size()-1; i++)
		len += (WayPointByIndex(i + 1)->v - WayPointByIndex(i)->v).Len();

	return len;
}


CWayPoint *CWay::WayPointByIndex(int index)
{
	list<CWayPoint *>::iterator i = way_points.begin();
	int c;

	for(c=0; c<index; c++)
	{
		i++;
		if(i==way_points.end())
			return 0;
	}

	return *i;
}

