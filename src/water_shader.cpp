#include "allinc.h"

CWaterShader water_shader;

void CWaterShader::Init(void)
{
	SetSource(water_shader_vert, water_shader_frag);
	CreateVertexShader();
	CreateFragmentShader();
	CreateProgram();
	LinkProgram();

	tex_uniform = glGetUniformLocationARB(program, "Texture");
	normal_uniform = glGetUniformLocationARB(program, "NormalTex");
	
	color_tex_uniform = glGetUniformLocationARB(program, "ColorTex");
	depth_tex_uniform = glGetUniformLocationARB(program, "DepthTex");

	anim_uniform = glGetUniformLocationARB(program, "Anim");

	UseNoShader();
}

void CWaterShader::SetTexture(GLuint tex, GLuint normal, GLuint color, GLuint depth)
{
	glUniform1iARB(tex_uniform, 6);
	glUniform1iARB(normal_uniform, 5);
	glUniform1iARB(color_tex_uniform, 7);
	glUniform1iARB(depth_tex_uniform, 8);

	glActiveTextureARB(GL_TEXTURE6_ARB);
	glBindTexture(GL_TEXTURE_2D, tex);

	glActiveTextureARB(GL_TEXTURE5_ARB);
	glBindTexture(GL_TEXTURE_2D, normal);

	glActiveTextureARB(GL_TEXTURE7_ARB);
	glBindTexture(GL_TEXTURE_2D, color);

	glActiveTextureARB(GL_TEXTURE8_ARB);
	glBindTexture(GL_TEXTURE_2D, depth);
}

void CWaterShader::SetAnim(float a)
{
	glUniform1fARB(anim_uniform, a);
}

