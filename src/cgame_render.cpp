#include "allinc.h"

void CGame::PaintLoading(void)
{
	if(!loading)
		return;

	Uint32 time = SDL_GetTicks();

	loading_anim.Play((float)(time - loading_tick) / 1000.0);

	if(time - loading_last_redraw < 20)
		return;

	loading_last_redraw = time;

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, display_width, display_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Set2DMatrix();

	UseNoShader();

	Blend();

	fonts[3]->setForegroundColor(1.0, 1.0, 1.0, 1.0);
	fonts[3]->setHorizontalJustification(OGLFT::Face::LEFT);
	fonts[3]->setVerticalJustification(OGLFT::Face::BOTTOM);

	fonts[3]->draw(10, display_height - fonts[3]->pointSize() + 5, "Loading...");

	loading_anim.PutToGL();

	glFlush();
	SDL_GL_SwapBuffers();
}

void CGame::BlurTexture(GLuint in, GLuint temp, GLuint out, int width, int height, float size)
{
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, blur.fbo);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, temp, 0);

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 0.0, 1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	blur_shader.BindShader();
	blur_shader.SetBlur(size, BLUR_VERTICAL);
	blur_shader.SetTexture(in, display_width, display_height);

	glColor4f(1.0, 1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
		blur_shader.SetTexCoord(Vec(0.0, 1.0));
		glVertex2f(-1.0, 1.0);

		blur_shader.SetTexCoord(Vec(0.0, 0.0));
		glVertex2f(-1.0, -1.0);

		blur_shader.SetTexCoord(Vec(1.0, 0.0));
		glVertex2f(1.0, -1.0);

		blur_shader.SetTexCoord(Vec(1.0, 1.0));
		glVertex2f(1.0, 1.0);
	glEnd();

	UseNoShader();

	glFlush();

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, out, 0);

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 0.0, 1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	blur_shader.BindShader();
	blur_shader.SetBlur(size, BLUR_HORIZONTAL);
	blur_shader.SetTexture(temp, display_width, display_height);

	glColor4f(1.0, 1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
		blur_shader.SetTexCoord(Vec(0.0, 1.0));
		glVertex2f(-1.0, 1.0);

		blur_shader.SetTexCoord(Vec(0.0, 0.0));
		glVertex2f(-1.0, -1.0);

		blur_shader.SetTexCoord(Vec(1.0, 0.0));
		glVertex2f(1.0, -1.0);

		blur_shader.SetTexCoord(Vec(1.0, 1.0));
		glVertex2f(1.0, 1.0);
	glEnd();

	UseNoShader();

	glFlush();

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, out);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

float CGame::Paint(void)
{
	Uint32 start_time, end_time;

	start_time = SDL_GetTicks();

	clip.v = Vec(0.0, 0.0, 0.0);
	clip.d = 0.0;

	//printf("-----4\n");
	//fire.context->Print();

	RenderShadows();
	RenderWater();

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, render.fbo);
	shader_enabled = 1;
	PaintGL(display_width * render.aa_level, display_height * render.aa_level);

	glFlush();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	UseNoShader();

	glActiveTextureARB(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, render.color_tex);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	BlurTexture(render.color_tex, render.color_tex_temp, render.color_tex_blurred,
				display_width * render.aa_level, display_height * render.aa_level,
				PAUSE_MENU_BLUR_SIZE * pause_menu->GetFade() * render.aa_level);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, display_width, display_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 0.0, 1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	display_shader.BindShader();
	display_shader.SetTexture(render.color_tex_blurred, render.depth_tex, 0.997);
	display_shader.SetGray(defeat.defeat ? defeat.anim / 5.0 : 0.0);

	glColor4f(1.0, 1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
		display_shader.SetTexCoord(Vec(0.0, 1.0));
		glVertex2f(-1.0, 1.0);

		display_shader.SetTexCoord(Vec(0.0, 0.0));
		glVertex2f(-1.0, -1.0);

		display_shader.SetTexCoord(Vec(1.0, 0.0));
		glVertex2f(1.0, -1.0);

		display_shader.SetTexCoord(Vec(1.0, 1.0));
		glVertex2f(1.0, 1.0);
	glEnd();

	glMatrixMode(GL_TEXTURE);
	glActiveTextureARB(GL_TEXTURE5);
	glLoadIdentity();
	glActiveTextureARB(GL_TEXTURE6);
	glLoadIdentity();
	glActiveTextureARB(GL_TEXTURE0);
	glMatrixMode(GL_MODELVIEW);

	UseNoShader();

	Paint2D(display_width, display_height);

	RenderField();

	glFlush();
	SDL_GL_SwapBuffers();

	end_time = SDL_GetTicks();

	return 1.0 / (((float)end_time - (float)start_time) / 1000.0);
}

void CGame::SetShadowTextureMatrix(void)
{
	static double modelView[16];
	static double projection[16];

	// This is matrix transform every coordinate x,y,z
	// x = x* 0.5 + 0.5
	// y = y* 0.5 + 0.5
	// z = z* 0.5 + 0.5
	// Moving from unit cube [-1,1] to [0,1]
	const GLdouble bias[16] = {
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0};

	// Grab modelview and transformation matrices
	glGetDoublev(GL_MODELVIEW_MATRIX, modelView);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);

	glMatrixMode(GL_TEXTURE);
	glActiveTextureARB(GL_TEXTURE5);

	glLoadIdentity();
	glLoadMatrixd(bias);

	// concatating all matrices into one.
	glMultMatrixd (projection);
	glMultMatrixd (modelView);

	// Go back to normal matrix mode
	glMatrixMode(GL_MODELVIEW);
}

void CGame::SetRenderTextureMatrix(void)
{
	// This is matrix transform every coordinate x,y,z
	// x = x* 0.5 + 0.5
	// y = y* 0.5 + 0.5
	// z = z* 0.5 + 0.5
	// Moving from unit cube [-1,1] to [0,1]
	const GLdouble bias[16] = {
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0};

	glMatrixMode(GL_TEXTURE);
	glActiveTextureARB(GL_TEXTURE7);

	glLoadIdentity();
	glLoadMatrixd(bias);

	// concatating all matrices into one.
	glMultMatrixd (matrices.projection_d);
	glMultMatrixd (matrices.model_view_d);

	// Go back to normal matrix mode
	glMatrixMode(GL_MODELVIEW);
}

void CGame::RenderShadows(void)
{
	shader_enabled = 0;
	UseNoShader();
	glEnable(GL_DEPTH_TEST);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, shadow.fbo);
	glViewport(0, 0, shadow.width, shadow.height);
	glClear(GL_DEPTH_BUFFER_BIT);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (double)shadow.width / (double)shadow.height, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(light.pos.x, light.pos.y, light.pos.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	SetShadowTextureMatrix();
	RenderScene();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	shader_enabled = 1;

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

}

void CGame::RenderWater(void)
{
	CVector cam_pos = GetCamPos();
       	CVector focus = GetFocus();
	cam_pos.y = 0.0 - cam_pos.y;
	focus.y = 0.0 - focus.y;

	water.width = display_width;
	water.height = display_height;

	shader_enabled = 1;

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, water.fbo);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK);

	glViewport(0, 0, water.width, water.height);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective((debug_mode ? 60.0 : debug_camera.angle) + 10.0, (double)water.width / (double)water.height, matrices.near_clip, matrices.far_clip); //Winkel, Seitenverhältnis, nearclip, farclip

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, focus.x, focus.y, focus.z, 0.0, -1.0, 0.0); //pos x, y, z, to x, y, z
	current_cam_pos = cam_pos; current_focus = focus;

	static double modelView[16];
	static double projection[16];

	const GLdouble bias[16] = {
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0};
	glGetDoublev(GL_MODELVIEW_MATRIX, modelView);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glMatrixMode(GL_TEXTURE);
	glActiveTextureARB(GL_TEXTURE6);
	glLoadIdentity();
	glLoadMatrixd(bias);
	glMultMatrixd (projection);
	glMultMatrixd (modelView);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_DEPTH_TEST);

	clip.v = Vec(0.0, -1.0, 0.0);
	clip.d = 0.0;

	map->PutEnvironmentToGL();

	RenderScene();
	PaintParticles();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	clip.v = Vec(0.0, 0.0, 0.0);
	clip.d = 0.0;
}

void CGame::RenderField(void)
{
	int i, j;
	GLfloat *towers;
	GLfloat *towers_range;
	int towers_count;
	CTower *t;

	for(t=first_tower, towers_count=0; t; t=t->chain_next)
		towers_count++;

	towers = new GLfloat[towers_count * 3];
	towers_range = new GLfloat[towers_count];

	for(i=0, t=first_tower; i<towers_count && t; i++, t=t->chain_next)
	{
		for(j=0; j<3; j++)
			towers[i*3+j] = t->pos.v[j];
		towers_range[i] = t->build_radius;
	}

	glEnable(GL_DEPTH_TEST);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, field.fbo);
	glDrawBuffer(GL_FRONT_LEFT);
	glViewport(0, 0, field.size, field.size);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(field.min.x, field.max.x, -field.max.z, -field.min.z, 0.0, field.max.y - field.min.y + 0.00001);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, field.max.y, 0.0,
			  0.0, -1.0, 0.0,
			  0.0, 0.0, -1.0); // top down

	field_shader.BindShader();
	field_shader.SetTowers(towers_count, towers, towers_range);
	for(i=0; i<map->ground_count; i++)
	{
		glBegin(GL_TRIANGLES);
		map->ground_t[i].v[0].PutToGL();
		map->ground_t[i].v[1].PutToGL();
		map->ground_t[i].v[2].PutToGL();
		glEnd();
	}

	glFlush();

	glReadBuffer(GL_FRONT_LEFT);
	glReadPixels(0, 0, field.size, field.size, GL_RED, GL_UNSIGNED_BYTE, field.data);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void CGame::DumpGLBuffer(GLuint tex, int width, int height)
{
	glBindTexture(GL_TEXTURE_2D, tex);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);
	glBindTexture(GL_TEXTURE_2D, 0);
}


void CGame::PaintGL(int width, int height)
{
	static const float att[] = {0.0, 1.0, 0.0};
	int i;
	std::map<int, std::map<int, CFieldNode *> *>::iterator ai;
	std::map<int, CFieldNode *>::iterator bi;
	list<CWayPoint *>::iterator ci;

	shader_enabled = 1;

	CVector cam_pos = GetCamPos();
	CVector _focus = GetFocus();

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK);

	glViewport(0, 0, width, height);
 	glGetIntegerv(GL_VIEWPORT, camera.viewport);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(!debug_mode ? 60.0 : debug_camera.angle, (double)width / (double)height, matrices.near_clip, matrices.far_clip); //Winkel, Seitenverhältnis, nearclip, farclip

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, _focus.x, _focus.y, _focus.z, 0.0, 1.0, 0.0); //pos x, y, z, to x, y, z
	current_cam_pos = cam_pos; current_focus = _focus;

	glGetFloatv(GL_MODELVIEW_MATRIX, matrices.model_view);
	glGetFloatv(GL_PROJECTION_MATRIX, matrices.projection);
	glGetDoublev(GL_MODELVIEW_MATRIX, matrices.model_view_d);
	glGetDoublev(GL_PROJECTION_MATRIX, matrices.projection_d);
	glGetIntegerv(GL_VIEWPORT, matrices.view_port);
	CombineMatrix4(matrices.model_view, matrices.projection, matrices.both);

	SetRenderTextureMatrix();

	glEnable(GL_DEPTH_TEST);

	map->PutEnvironmentToGL();

	RenderScene();

	DumpGLBuffer(render.color_tex_dump, display_width * render.aa_level, display_height * render.aa_level);
	DumpGLBuffer(render.depth_tex_dump, display_width * render.aa_level, display_height * render.aa_level);

	glColor4f(1.0, 1.0, 1.0, 1.0);
	Blend();
	water_shader.BindShader();
	water_shader.SetTexture(water.tex, water.normal_tex, render.color_tex_dump, render.depth_tex_dump);
	water_shader.SetAnim(water.anim);
	map->PutWaterToGL();

	PaintParticles();

	UseNoShader();
	glDisable(GL_LIGHTING);

	if(field.show)
	{
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, field.color_tex);
		glColor4f(1.0, 1.0, 1.0, 0.5);
		for(i=0; i<map->ground_count; i++)
		{
			glBegin(GL_TRIANGLES);
			glTexCoord2fv(field.GetTexCoord(map->ground_t[i].v[0]).v);
			map->ground_t[i].v[0].PutToGL();
			glTexCoord2fv(field.GetTexCoord(map->ground_t[i].v[1]).v);
			map->ground_t[i].v[1].PutToGL();
			glTexCoord2fv(field.GetTexCoord(map->ground_t[i].v[2]).v);
			map->ground_t[i].v[2].PutToGL();
			glEnd();
		}

		glDisable(GL_TEXTURE_2D);
		glColor4f(1.0, 0.0, 0.0, 0.5);
		glPointSize(5.0);
		glBegin(GL_POINTS);
		for(ai = map->field_nodes.begin(); ai != map->field_nodes.end(); ai++)
		{
			for(bi = (*ai).second->begin(); bi != (*ai).second->end(); bi++)
			{
				(*bi).second->pos.PutToGL();
			}
		}
		glEnd();

		if(map->way)
		{
			glColor4f(0.0, 1.0, 0.0, 0.5);
			glLineWidth(5.0);
			glBegin(GL_LINE_STRIP);
			for(ci = map->way->way_points.begin(); ci != map->way->way_points.end(); ci++)
				(*ci)->v.PutToGL();
			glEnd();
		}
	}
	glEnable(GL_DEPTH_TEST);
	if(selected_tower && game_mode != GAME_MODE_NONE)
		selected_tower->PutRangeToGL();

	if(debug_mode)
		PaintAxes();

	if(debug_mode)
	{
		SetColor(GL_DIFFUSE, 0.0, 1.0, 0.0);
		glPointSize(16.0);
		glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, att);
		glBegin(GL_POINTS);
		glEnd();
	}
}

void CGame::Paint2D(int width, int height)
{
	CVector p;
	CEnemy *e;

	UseNoShader();

	Set2DMatrix();

	for(e=first_enemy; e; e=e->chain_next)
	{
		if(e->dead)
			continue;
		p = Project(e->pos + Vec(0.0, 1.0, 0.0));
		p.z = Dot((e->pos + Vec(0.0, 1.0, 0.0)) - GetCamPos(), GetFocus() - GetCamPos());
		PaintLifeDisplay(p, e->life / e->max_life, e->GetAlpha());
	}

	game_hud->PutToGL();

	if(defeat.defeat)
	{
		Blend();
		defeat_sprite.SetAlpha(defeat.anim / 5.0);
		defeat_sprite.PutToGL();
		glColor4f(1.0, 1.0, 1.0, 1.0);
	}

	pause_menu->PutToGL();

	if(debug_mode)
	{
		Blend();
		fonts[0]->setBackgroundColor(0.0, 0.0, 0.0, 0.0);
		fonts[0]->setForegroundColor(1.0, 0.0, 0.0, 1.0);
		fonts[0]->setVerticalJustification(OGLFT::Face::TOP);
		fonts[0]->setHorizontalJustification(OGLFT::Face::LEFT);
		fonts[0]->draw(3, 3, "Debug");
	}
	else
		mouse_cursor.PutToGL();

}


void CGame::PaintParticles(void)
{
	static const CVector color = Vec(0.8, 0.3, 0.1);
	float alpha;
	float age;
	float life_time;
	CVector focus = current_focus - current_cam_pos;
	focus.Normalize();
	focus = focus + GetCamPos();

	fire.context->Sort(current_cam_pos);

	list<CParticle *> *plist = fire.context->GetList();
	list<CParticle *>::iterator i;
	CParticle *p;

	UseNoShader();
	glDisable(GL_LIGHTING);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_COLOR, GL_DST_COLOR);

	//for(i=0; i<fire.context->Count(); i++)
	for(i=plist->begin(); i!=plist->end(); i++)
	{
		p = *i;

		alpha = p->a;
		age = p->age * fire.speed;
		life_time = p->life_time;

		if(age < life_time)
		{
			if(age < 0.2)
				alpha *= age / 0.2;
			if(age > life_time - 0.5)
				alpha *= min(1.0 - ((age - (life_time - 0.5)) / 0.5), 1.0);

			glBindTexture(GL_TEXTURE_2D, p->tex);
			glColor4f(pvec(color), alpha);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			_glPaintSprite(p->pos, 0.4, current_cam_pos);
		}

		if(age > life_time - 0.5)
		{
			alpha = 0.2;
			alpha *= 1.0 - max(min(((age - (life_time - 0.5)) / 3.0), 1.0), 0.0);
			alpha *= max(min(((age - (life_time - 0.5)) / 0.4), 1.0), 0.0);
			glBindTexture(GL_TEXTURE_2D, templates.textures.particles.smoke);
			glColor4f(0.0, 0.0, 0.0, alpha);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			_glPaintSprite(p->pos, 0.25, current_cam_pos);
		}

	}
	Blend();
}

void CGame::RenderScene(void)
{
	CEnemy *e;
	CTower *t;
	CShot *s;

	CMesh::LoadIdentity();

	face_shader.BindShader();
	face_shader.SetLight(light.pos, light.color);
	face_shader.SetTwoSide(0);
	face_shader.SetBorder(0, Vec(0.0, 0.0), Vec(0.0, 0.0));
	face_shader.SetAmbientColor(Vec(0.1, 0.1, 0.1));
	face_shader.SetShadow(1, shadow.tex, Vec(1.0 / (float)shadow.width, 1.0 / (float)shadow.height));
	face_shader.SetClip(clip.v, clip.d);

	map->PutToGL();

	for(e=first_enemy; e; e=e->chain_next)
		e->PutToGL();
	for(t=first_tower; t; t=t->chain_next)
		t->PutToGL();
	for(s=first_shot; s; s=s->chain_next)
		s->PutToGL();
}

void CGame::PaintLifeDisplay(CVector p, float l, float alpha)
{
	if(p.z < 0.0)
		return;

	int width;

	if(p.z < 7.0)
		width = 100;
	else
		width = 50;

	int height = 5;
	int border = 1;
	int life = (int)((float)width * l);
	CVector color = Mix(Vec(1.0, 0.0, 0.0), Vec(0.0, 1.0, 0.0), l);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor4f(0.0, 0.0, 0.0, 0.5 * alpha);
	glBegin(GL_QUADS);
	glVertex2i((int)p.x - width / 2 - border, (int)p.y - height / 2 - border);
	glVertex2i((int)p.x - width / 2 - border, (int)p.y + height / 2 + border);
	glVertex2i((int)p.x + width / 2 + border, (int)p.y + height / 2 + border);
	glVertex2i((int)p.x + width / 2 + border, (int)p.y - height / 2 - border);
	glEnd();

	glColor4f(color.x, color.y, color.z, 1.0 * alpha);
	glBegin(GL_QUADS);
	glVertex2i((int)p.x - width / 2, (int)p.y - height / 2);
	glVertex2i((int)p.x - width / 2, (int)p.y + height / 2);
	glVertex2i((int)p.x - width / 2 + life, (int)p.y + height / 2);
	glVertex2i((int)p.x - width / 2 + life, (int)p.y - height / 2);
	glEnd();

}

CVector CGame::Project(CVector v)
{
	GLdouble x, y, z;
	CVector r;
	gluProject(v.x, v.y, v.z, matrices.model_view_d, matrices.projection_d, matrices.view_port, &x, &y, &z);
	r = Vec(x, y, z);
	r *= 1.0 / render.aa_level;
	r.y = (float)display_height - r.y;
	return r;
}
