#include "allinc.h"

CEnemy::CEnemy(CGame *game)
{
	to_delete = 0;

	this->game = game;
	way = game->GetMap()->GetWay();
	chain_next = game->first_enemy;
	game->first_enemy = this;

	mesh = new CMeshHandler(&game->templates.enemy);
	dir = Vec(0.0, 0.0, 1.0);
	aim_pos = Vec(0.0, 0.5, 0.0);

	speed = 6.0;
	slow_speed = 3.0;

	mesh->SetAnimationMode(true);
	mesh->SetAnimationLoop(true);
	mesh->SetAnimation("go");
	way_dist = 0.0;
	run = 0;
	life = 4.0;
	max_life = 4.0;
	slow_time = 0.0;
	stunned_time = 0.0;
	dead = 0;
	burning = 0;
	burn_time = 0.0;
	on_ground = true;

	SelectFireSource();
}

CEnemy::~CEnemy(void)
{
	CEnemy *e;

	if(game->first_enemy == this)
		game->first_enemy = chain_next;
	else
		for(e=game->first_enemy; e; e=e->chain_next)
			if(e->chain_next == this)
				e->chain_next = chain_next;
	if(fire_source != -1)
	{
		game->fire.free_src[fire_source] = 0;
		game->fire.context->ClearSource(fire_source);
	}

	delete mesh;
}

void CEnemy::CalculatePosition(void)
{
	if(!game->GetMap())
		return;

	pos = way->PositionByWayDist(way_dist, dir, 2.0);
	pos = game->GetMap()->ProjectOnGround(pos.xz(), &on_ground);
	rot.x = asin(dir.y);
	rot.y = acos(dir.z);

	if(dir.x < 0.0)
		rot.y = M_PI * 2 - rot.y;
}

void CEnemy::Run(float time)
{
	slow_time = max(slow_time - time, 0.0);
	slowed = slow_time == 0.0 ? 0 : 1;
	stunned_time = max(stunned_time - time, 0.0);
	stunned = stunned_time == 0.0 ? 0 : 1;
	burn_time = max(burn_time - time, 0.0);
	burning = burn_time == 0.0 ? 0 : 1;

	if(!dead)
	{
		if(burning)
		{
			life = max(life - 0.6 * time, 0.0);
		}

		if(!stunned)
		{
			real_speed = slowed ? slow_speed : speed;
			way_dist += real_speed * time;
		}
		else
			real_speed = 0.0;


		if(way_dist >= way->GetLength())
		{
			way_dist = way->GetLength();
			to_delete = 1;
			game->lifes -= 1;
		}
		if(life <= 0.0)
		{
			life = 0.0;
			dead = 1;
			game->mana += 1;
			mesh->SetAnimationLoop(false);
			mesh->SetAnimation("die");
			mesh->SetAnimationTime(0.0);
		}
	}
	else
	{
		if(mesh->GetAnimationFinished())
			to_delete = 1;
	}

	CalculatePosition();

	if(!dead)
	{
		if(!stunned)
		{
			mesh->PlayAnimation(slowed ? time * (slow_speed / speed) : time);
		}
		if(burning)
		{
			if(fire_source != -1)
			{
				game->fire.context->Source(fire_source, 100.0 * game->fire.speed, pos + Vec(0.0, 1.0, 0.0), 0.5);
				game->fire.context->Velocity(fire_source, real_speed * dir + Vec(0.0, 1.0, 0.0) * game->fire.speed, 0.3);
			}
		}
		else if(fire_source != -1)
			game->fire.context->ClearSource(fire_source);

	}
	else
	{
		mesh->PlayAnimation(time);
		if(fire_source != -1)
			game->fire.context->ClearSource(fire_source);
	}

	run = 1;
}

float CEnemy::GetAlpha(void)
{
	float dist = sqrt(min(	(game->GetMap()->start - pos.xz()).SquaredLen(),
								(game->GetMap()->end - pos.xz()).SquaredLen()));

	if(dist < fade_dist)
		return dist / fade_dist;

	return 1.0;
}

void CEnemy::PutToGL(void)
{
	float alpha = GetAlpha();
	CVector color = Vec(1.0, 1.0, 1.0);

	if(!on_ground)
		return;

	CMesh::LoadIdentity();
	CMesh::Translate(pos);
	CMesh::RotateX(rot.x);
	CMesh::RotateY(rot.y);
	if(slowed)
		color = Vec(0.6, 1.0, 0.6);
	CMesh::Color(color, alpha);
	if(alpha < 0.9)
		glDisable(GL_DEPTH_TEST);
	mesh->PutToGL(game->GetCamPos());
	glEnable(GL_DEPTH_TEST);
	CMesh::LoadIdentity();
}
