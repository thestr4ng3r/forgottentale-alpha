#include "allinc.h"

CGameHUD::CGameHUD(CGame *game)
{
	this->game = game;
	program = game->program;
}

void CGameHUD::Load(void)
{
	arrow_tower_button.LoadImage("./images/hud/arrow_button.png");
	slow_tower_button.LoadImage("./images/hud/slow_button.png");
	ice_tower_button.LoadImage("./images/hud/ice_button.png");
	fire_tower_button.LoadImage("./images/hud/fire_button.png");
	stone_tower_button.LoadImage("./images/hud/stone_button.png");
	enemy_button.LoadImage("./images/hud/enemy_button.png");
	life_display.LoadImage("./images/hud/life_mana_display.png");
	pointer.LoadImage("./images/pointer.png");
}

void CGameHUD::Refresh(void)
{
	const int space = 10;
	arrow_tower_button.SetPosition(space, display_height - arrow_tower_button.Height() - space);
	slow_tower_button.SetPosition(arrow_tower_button.X() + arrow_tower_button.Width() + space,
			display_height - slow_tower_button.Height() - space);
	ice_tower_button.SetPosition(slow_tower_button.X() + slow_tower_button.Width() + space,
			display_height - ice_tower_button.Height() - space);
	fire_tower_button.SetPosition(ice_tower_button.X() + ice_tower_button.Width() + space,
			display_height - fire_tower_button.Height() - space);
	stone_tower_button.SetPosition(fire_tower_button.X() + fire_tower_button.Width() + space,
				display_height - stone_tower_button.Height() - space);
	enemy_button.SetPosition(display_width - enemy_button.Width() - space,
			display_height - enemy_button.Height() - space);
	life_display.SetPosition(display_width - life_display.Width(), 0);
	pointer.SetPosition(display_width / 2 - pointer.Width(), display_height / 2 - pointer.Height());
}

void CGameHUD::PutToGL(void)
{
	char text[10];

	arrow_tower_button.PutToGL();
	slow_tower_button.PutToGL();
	ice_tower_button.PutToGL();
	fire_tower_button.PutToGL();
	stone_tower_button.PutToGL();
	enemy_button.PutToGL();
	life_display.PutToGL();
	if(game->GetDebugMode())
		pointer.PutToGL();

	Blend();
	fonts[1]->setForegroundColor(1.0, 1.0, 1.0);
	fonts[1]->setHorizontalJustification(OGLFT::Face::RIGHT);
	fonts[1]->setVerticalJustification(OGLFT::Face::TOP);
	snprintf(text, 9, "%d", game->lifes);
	fonts[1]->draw(display_width - 26, 8, text);

	snprintf(text, 9, "%d", game->mana);
	fonts[1]->draw(display_width - 26, 32, text);

	if(game->show_fps)
	{
		fonts[1]->setForegroundColor(1.0, 1.0, 1.0);
		fonts[1]->setHorizontalJustification(OGLFT::Face::LEFT);
		fonts[1]->setVerticalJustification(OGLFT::Face::TOP);
		snprintf(text, 9, "FPS: %f", game->program->fps);
		fonts[1]->draw(10, 20, text);
	}
}

void CGameHUD::OnMouseDown(int x, int y)
{
	if(arrow_tower_button.PointCollision(x, y))
		game->InitBuildTower(TOWER_TYPE_ARROW);
	if(slow_tower_button.PointCollision(x, y))
		game->InitBuildTower(TOWER_TYPE_SLOW);
	if(ice_tower_button.PointCollision(x, y))
		game->InitBuildTower(TOWER_TYPE_ICE);
	if(fire_tower_button.PointCollision(x, y))
		game->InitBuildTower(TOWER_TYPE_FIRE);
	if(stone_tower_button.PointCollision(x, y))
		game->InitBuildTower(TOWER_TYPE_STONE);
	if(enemy_button.PointCollision(x, y))
		game->CreateEnemy();
}
