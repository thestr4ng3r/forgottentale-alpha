#include "allinc.h"

CDisplayShader display_shader;

void CDisplayShader::Init(void)
{
	SetSource(display_shader_vert, display_shader_frag);
	CreateVertexShader();
	CreateFragmentShader();
	CreateProgram();
	LinkProgram();

	color_tex_uniform = glGetUniformLocationARB(program, "Texture");
	depth_tex_uniform = glGetUniformLocationARB(program, "DepthTexture");
	depth_threshold_uniform = glGetUniformLocationARB(program, "DepthThreshold");
	gray_uniform = glGetUniformLocationARB(program, "Gray");

	UseNoShader();
}

void CDisplayShader::SetTexCoord(CVector2 c)
{
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glTexCoord2fv(c.v);
}

void CDisplayShader::SetGray(float g)
{
	glUniform1fARB(gray_uniform, g);
}

void CDisplayShader::SetTexture(GLuint color, GLuint depth, float thresh)
{
	glUniform1iARB(color_tex_uniform, 0);
	glUniform1iARB(depth_tex_uniform, 1);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, color);
	
	glActiveTextureARB(GL_TEXTURE1_ARB);
	glBindTexture(GL_TEXTURE_2D, depth);

	glUniform1fARB(depth_threshold_uniform, thresh);
}

