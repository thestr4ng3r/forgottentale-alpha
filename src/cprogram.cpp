
#include "allinc.h"

void CProgram::InitGlobal(void)
{
	int i;

	fonts[0] = new OGLFT::Translucent("./fonts/font01.ttf", 12);
	fonts[1] = new OGLFT::Translucent("./fonts/font02.ttf", 17);
	fonts[2] = new OGLFT::Translucent("./fonts/font01.ttf", 72);
	fonts[3] = new OGLFT::Translucent("./fonts/font03.ttf", 17);

	for(i=0; i<FONT_COUNT; i++)
	{
		if(!fonts[i])
		{
			printf("Could not load font %d!\n", i);
			exit(0);
		}
	}

	cursor = IMG_Load("images/cursor.png");
	cursor_gl = CreateGLImage(cursor);
	rotate_cursor = IMG_Load("images/rotate_cursor.png");
	rotate_cursor_gl = CreateGLImage(rotate_cursor);
	move_cursor = IMG_Load("images/move_cursor.png");
	move_cursor_gl = CreateGLImage(move_cursor);

	CGame::InitGlobalGame();
}

CProgram::CProgram(void)
{
	mode = 0;
	last_tick = 0.0;
	fps = 0.0;
}

CProgram::~CProgram(void)
{
	ResetMode();
}

void CProgram::Init(void)
{
#ifdef DEBUG_MODE
	ChangeToGame();
#else
	ChangeToIntro();
#endif
}

void CProgram::Run(void)
{
	switch(mode)
	{
		case PROGRAM_MODE_GAME:
			game->Run();
			break;
		case PROGRAM_MODE_INTRO:
			intro->Run();
			break;
	}
}

void CProgram::Paint(void)
{
	switch(mode)
	{
		case PROGRAM_MODE_GAME:
			fps = game->Paint();
			break;
		case PROGRAM_MODE_INTRO:
			intro->Paint();
			break;
	}
}

void CProgram::OnEvent(SDL_Event event)
{
	switch(mode)
	{
		case PROGRAM_MODE_GAME:
			game->OnEvent(event);
			break;
	}
}

void CProgram::ResetMode(void)
{
	if(mode == PROGRAM_MODE_GAME)
		delete game;
	mode = 0;
}

void CProgram::ChangeToGame(void)
{
	if(mode == PROGRAM_MODE_GAME)
		return;
	ResetMode();
	mode = PROGRAM_MODE_GAME;
	game = new CGame(this);
	game->Init();
}

void CProgram::ChangeToIntro(void)
{
	if(mode == PROGRAM_MODE_INTRO)
		return;
	ResetMode();
	mode = PROGRAM_MODE_INTRO;
	intro = new CProgramIntro(this);
	intro->Init();
}


CProgramIntro::CProgramIntro(CProgram *program)
{
	this->program = program;
	time = 0.0;
}

void CProgramIntro::Init(void)
{
	time = 0.0;
}

void CProgramIntro::Run(void)
{
	time += program->last_tick;
	if(time > length)
		program->ChangeToGame();
}

const char *intro_text[] =
		{ "This is an Alpha Version of Forgotten Tale.\n",
		  "It does not yet have the full functionality of the final game.\n",
		  "\n",
		  "For further information, go to http://www.metallic-entertainment.com/forgottentale" };
const int intro_text_lines = 4;

void CProgramIntro::Paint(void)
{
	int i, y;
	float fade;

	if(time < fade_length)
		fade = time / fade_length;
	else if(time > length - fade_length)
		fade = (length - time) / fade_length;
	else fade = 1.0;

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, display_width, display_height);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	UseNoShader();

	Set2DMatrix();
	Blend();

	fonts[3]->setForegroundColor(1.0, 1.0, 1.0, fade);
	fonts[3]->setHorizontalJustification(OGLFT::Face::CENTER);
	fonts[3]->setVerticalJustification(OGLFT::Face::MIDDLE);

	for(i=0; i<intro_text_lines; i++)
	{
		y = (display_height * 0.5) -
				((intro_text_lines * fonts[3]->pointSize()) + (5 * (fonts[3]->pointSize() - 1))) * 0.5
				+ i * (fonts[3]->pointSize() + 5);
		fonts[3]->draw(display_width / 2, y, intro_text[i]);
	}

	glFlush();
	SDL_GL_SwapBuffers();
}













