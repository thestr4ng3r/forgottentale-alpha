#include "allinc.h"

#define MAX_TOWERS 200
#define MAX_WP 200

CFieldShader field_shader;

void CFieldShader::Init(void)
{
	SetSource(field_shader_vert, field_shader_frag);
	CreateVertexShader();
	CreateFragmentShader();
	CreateProgram();
	LinkProgram();

	towers_uniform = glGetUniformLocationARB(program, "Towers");
	towers_range_uniform = glGetUniformLocationARB(program, "TowersRange");
	towers_count_uniform = glGetUniformLocationARB(program, "TowersCount");

	UseNoShader();
}

void CFieldShader::SetTowers(int count, GLfloat *data, GLfloat *range)
{
	glUniform3fvARB(towers_uniform, min(count, MAX_TOWERS), data);
	glUniform1fvARB(towers_range_uniform, min(count, MAX_TOWERS), range);
	glUniform1iARB(towers_count_uniform, min(count, MAX_TOWERS));
}













