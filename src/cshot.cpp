#include "allinc.h"

CShot::CShot(CGame *game, CEnemy *aim, CTower *tower)
{
	to_delete = 0;

	this->game = game;
	chain_next = game->first_shot;
	game->first_shot = this;

	this->aim = aim;
	type = tower->type;

	damage = 0.0;
	slow_time = 0.0;
	stunned_time = 0.0;
	burn_time = 0.0;

	switch(type)
	{
		case TOWER_TYPE_ARROW:
			mesh = new CMeshHandler(&game->templates.towers.arrow.shot);
			damage = 1.0;
			break;
		case TOWER_TYPE_SLOW:
			mesh = new CMeshHandler(&game->templates.towers.slow.shot);
			damage = 0.1;
			slow_time = 2.0;
			break;
		case TOWER_TYPE_ICE:
			mesh = new CMeshHandler(&game->templates.towers.ice.shot);
			damage = 0.4;
			stunned_time = 0.8;
			break;
		case TOWER_TYPE_FIRE:
			mesh = new CMeshHandler(&game->templates.towers.fire.shot);
			damage = 0.2;
			burn_time = 3.0;
			break;
	}

	pos = tower->pos + tower->shoot_pos;
	speed = 10.0;
	hit = 0;
}

CShot::~CShot(void)
{
	CShot *e;

	if(game->first_shot == this)
		game->first_shot = chain_next;
	else
		for(e=game->first_shot; e; e=e->chain_next)
			if(e->chain_next == this)
				e->chain_next = chain_next;
}

void CShot::Run(float time)
{
	dir = (aim->pos + aim->aim_pos) - pos;
	float dist = dir.Len();
	float t_speed = speed * time;
	dir.Normalize();
	pos += dir * t_speed;
	if(Dot(dir, Vec(1.0, 0.0, 0.0)) > 0.9)
		top = Cross(dir, Vec(0.0, 0.0, 1.0));
	else
		top = Cross(dir, Vec(1.0, 0.0, 0.0));
	top.Normalize();
	//top = Cross(dir, Cross(dir, Vec(0.0, 1.0, 0.0)));
	if(dist < t_speed)
	{
		if(!hit)
		{
			hit = 1;
			aim->life -= damage;
			aim->slow_time = max(aim->slow_time, slow_time);
			aim->stunned_time = max(aim->stunned_time, stunned_time);
			aim->burn_time = max(aim->burn_time, burn_time);
		}
		to_delete = 1;
	}
}

void CShot::PutToGL(void)
{
	if(to_delete || hit)
		return;

	if(dir.SquaredLen() < 0.9 || dir.SquaredLen() > 1.1)
		return;

	CMesh::LoadIdentity();
	CMesh::Translate(pos);
	CMesh::SetYZ(top, dir);
	mesh->PutToGL(game->GetCamPos());
	CMesh::LoadIdentity();
}
