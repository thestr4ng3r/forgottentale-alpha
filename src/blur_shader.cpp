#include "allinc.h"

CBlurShader blur_shader;

void CBlurShader::Init(void)
{
	SetSource(blur_shader_vert, blur_shader_frag);
	CreateVertexShader();
	CreateFragmentShader();
	CreateProgram();
	LinkProgram();

	texture_uniform = glGetUniformLocationARB(program, "Texture");
	width_uniform = glGetUniformLocationARB(program, "TargetWidth");
	height_uniform = glGetUniformLocationARB(program, "TargetHeight");
	blur_size_uniform = glGetUniformLocationARB(program, "BlurSize");
	direction_uniform = glGetUniformLocationARB(program, "BlurDirection");

	UseNoShader();
}

void CBlurShader::SetTexture(GLuint tex, int width, int height)
{
	glUniform1iARB(texture_uniform, 0);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, tex);

	glUniform1fARB(width_uniform, (float)width);
	glUniform1fARB(height_uniform, (float)height);
}

void CBlurShader::SetTexCoord(CVector2 v)
{
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glTexCoord2fv(v.v);
}

void CBlurShader::SetBlur(float size, int dir)
{
	int _dir;

	if(dir != BLUR_HORIZONTAL && dir != BLUR_VERTICAL)
		_dir = BLUR_HORIZONTAL;
	else
		_dir = dir;

	glUniform1fARB(blur_size_uniform, size);
	glUniform1iARB(direction_uniform, _dir);
}
