#include "allinc.h"

CMusicPlayer::CMusicPlayer(void)
{
	current_song = 0;
	songs = 0;
	songs_count = 0;
}

void CMusicPlayer::Init(void)
{
	const char *song_files[] = { "./music/score01.ogg", "./music/score02.ogg" };
	int i;

	songs_count = 2;
	songs = new Mix_Music *[songs_count];

	for(i=0; i<songs_count; i++)
	{
		songs[i] = Mix_LoadMUS(song_files[i]);
		if(!songs[i])
			printf("Could not load song \"%s\"\n", song_files[i]);
	}
}

void CMusicPlayer::Play(void)
{
	int last;

#ifdef DEBUG_MODE
	return;
#endif

	if(songs_count == 0)
		return;

	last = current_song;

	if(songs_count > 1)
		while(current_song == last)
			current_song = rand() % songs_count;
	else
		current_song = 0;

	Mix_PlayMusic(songs[current_song], 0);
}

void CMusicPlayer::Pause(void)
{
	Mix_PauseMusic();
}
