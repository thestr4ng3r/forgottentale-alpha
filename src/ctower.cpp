#include "allinc.h"

CTower::CTower(CGame *game, int t)
{
	this->game = game;
	chain_next = game->first_tower;
	game->first_tower = this;

	type = t;

	switch(type)
	{
		case TOWER_TYPE_ARROW:
			mesh = new CMeshHandler(&game->templates.towers.arrow.tower);
			shoot_time = 0.3;
			break;
		case TOWER_TYPE_SLOW:
			mesh = new CMeshHandler(&game->templates.towers.slow.tower);
			shoot_time = 0.3;
			break;
		case TOWER_TYPE_ICE:
			mesh = new CMeshHandler(&game->templates.towers.ice.tower);
			shoot_time = 2.0;
			break;
		case TOWER_TYPE_FIRE:
			mesh = new CMeshHandler(&game->templates.towers.fire.tower);
			shoot_time = 2.0;
			break;
		case TOWER_TYPE_STONE:
			mesh = new CMeshHandler(&game->templates.towers.stone.tower);
			break;
	}

	pos = Vec(0.0, 0.0, 0.0);
	rot = ((float)rand() / (float)RAND_MAX) * 2 * M_PI;
	shoot_pos = Vec(0.0, 2.3, 0.0);
	time_r = 0.0;
	range = 5.0;
	color_fade_active = false;

	switch(type)
	{
		case TOWER_TYPE_ARROW:
			power = 1.0;
			break;
		case TOWER_TYPE_SLOW:
			power = 2.0;
			break;
	}

	switch(type)
	{
		case TOWER_TYPE_STONE:
			build_radius = 1.5;
			break;
		default:
			build_radius = 0.7;
			break;
	}
	enemy_crit = ENEMY_CRIT_FIRST;
}

CTower::~CTower(void)
{
	CTower *e;

	if(game->first_tower == this)
		game->first_tower = chain_next;
	else
		for(e=game->first_tower; e; e=e->chain_next)
			if(e->chain_next == this)
				e->chain_next = chain_next;
}

CTower *CTower::CreateTower(CGame *game, int t)
{
	switch(t)
	{
		case TOWER_TYPE_ARROW:
			return (CTower *)(new CArrowTower(game));
		case TOWER_TYPE_SLOW:
			return (CTower *)(new CSlowTower(game));
		case TOWER_TYPE_ICE:
			return (CTower *)(new CIceTower(game));
		case TOWER_TYPE_FIRE:
			return (CTower *)(new CFireTower(game));
		case TOWER_TYPE_STONE:
			return (CTower *)(new CStoneTower(game));
		default:
			return new CTower(game, t);
	}
}

void CTower::ColorFade(CVector color, float length, float max_begin, float max_end)
{
	fade_color = color;
	color_fade_length = length;
	color_fade_time = 0.0;
	color_fade_active = true;
	color_fade_max_begin = max_begin;
	color_fade_max_end = max_end;
}

void CTower::Run(float time)
{
	if(game->selected_tower == this && game->game_mode == GAME_MODE_BUILD_TOWER)
		return;

	time_r -= time;
	if(time_r < 0.0)
		time_r = 0.0;
}

void CTower::PutToGL(void)
{
	CVector color;
	float alpha;
	float fade_mix;

	CMesh::LoadIdentity();
	CMesh::Translate(pos);
	CMesh::RotateY(rot);

	color = Vec(1.0, 1.0, 1.0);
	alpha = 1.0;

	if(game->selected_tower == this)
	{
		switch(game->game_mode)
		{
			case GAME_MODE_BUILD_TOWER:
				if(valid_pos)
				{
					color = Vec(1.0, 1.0, 1.0);
					alpha = 0.5;
				}
				else
				{
					color = Vec(1.0, 0.2, 0.2);
					alpha = 0.5;
				}
				break;
			default:
				color = Vec(2.0, 0.5, 0.5);
				alpha = 1.0;
		}
	}

	if(color_fade_active)
	{
		color_fade_time += game->program->last_tick;
		if(color_fade_time > color_fade_length)
			color_fade_active = false;
	}

	if(color_fade_active)
	{
		if(color_fade_time < color_fade_max_begin)
			fade_mix = color_fade_time / color_fade_max_begin;
		else if(color_fade_time > color_fade_max_begin && color_fade_time < color_fade_max_end)
			fade_mix = 1.0;
		else // color_fade_time > color_fade_max_end
			fade_mix = 1.0 - (color_fade_time - color_fade_max_end) / (color_fade_length - color_fade_max_end);

		color *= 1.0 - fade_mix;
		color += fade_color * fade_mix;
	}

	CMesh::Color(color, alpha);

	mesh->PutToGL(game->GetCamPos());
	if(shader_enabled)
		CMesh::LoadIdentity();
	else
	{
		glPopMatrix();
		glPopMatrix();
	}
}

void CTower::PutRangeToGL(void)
{
	GLUquadric *q;

	if(type == TOWER_TYPE_STONE)
		return;

	q = gluNewQuadric();

	UseNoShader();
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glColor4f(1.0, 1.0, 1.0, 0.2);

	glPushMatrix();
	glTranslatef(pos.x, pos.y, pos.z);
	gluSphere(q, range, 32, 32);
	glPopMatrix();
}

void CStoneTower::Run(float time)
{
	CTower::Run(time);
}

void CArrowTower::Run(float time)
{
	CTower::Run(time);

	CEnemy *e;
	CEnemy *best_enemy;
	float best_crit;
	float e_dist;

	if(time_r == 0.0)
	{
		best_crit = -(float)INFINITY;
		best_enemy = 0;

		for(e=game->first_enemy; e; e=e->chain_next)
		{
			if(!e->run || e->dead)
				continue;

			e_dist = ((e->pos + e->aim_pos) - (pos + shoot_pos)).Len();
			if(e_dist <= range)
			{
				switch(enemy_crit)
				{
					case ENEMY_CRIT_FIRST:
						if(e->way_dist > best_crit)
						{
							best_crit = e->way_dist;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_LAST:
						if(-e->way_dist > best_crit)
						{
							best_crit = -e->way_dist;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_WEAKEST:
						if(-e->life > best_crit)
						{
							best_crit = -e->life;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_STRONGEST:
						if(e->life > best_crit)
						{
							best_crit = e->life;
							best_enemy = e;
						}
						break;
					default:
						best_enemy = e;
						break;
				}
			}
		}
		if(best_enemy)
		{
			new CShot(game, best_enemy, this);
			time_r = shoot_time;
		}
	}

}

void CSlowTower::Run(float time)
{
	CTower::Run(time);

	CEnemy *e;
	CEnemy *best_enemy;
	float best_crit;
	float e_dist;

	if(time_r == 0.0)
	{
		best_crit = -(float)INFINITY;
		best_enemy = 0;

		for(e=game->first_enemy; e; e=e->chain_next)
		{
			if(!e->run || e->dead)
				continue;

			e_dist = ((e->pos + e->aim_pos) - (pos + shoot_pos)).Len();
			if(e_dist <= range)
			{
				if(-e->slow_time > best_crit)
				{
					best_crit = -e->slow_time;
					best_enemy = e;
				}
			}
		}
		if(best_enemy)
		{
			new CShot(game, best_enemy, this);
			time_r = shoot_time;
		}
	}

}

void CIceTower::Run(float time)
{
	CTower::Run(time);

	CEnemy *e;
	CEnemy *best_enemy;
	float best_crit;
	float e_dist;

	if(time_r == 0.0)
	{
		best_crit = -(float)INFINITY;
		best_enemy = 0;

		for(e=game->first_enemy; e; e=e->chain_next)
		{
			if(!e->run || e->dead)
				continue;

			e_dist = ((e->pos + e->aim_pos) - (pos + shoot_pos)).Len();
			if(e_dist <= range)
			{
				switch(enemy_crit)
				{
					case ENEMY_CRIT_FIRST:
						if(e->way_dist > best_crit)
						{
							best_crit = e->way_dist;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_LAST:
						if(-e->way_dist > best_crit)
						{
							best_crit = -e->way_dist;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_WEAKEST:
						if(-e->life > best_crit)
						{
							best_crit = -e->life;
							best_enemy = e;
						}
						break;
					case ENEMY_CRIT_STRONGEST:
						if(e->life > best_crit)

	if(game->selected_tower == this && game->game_mode == GAME_MODE_BUILD_TOWER)
		return;

	time_r -= time;
	if(time_r < 0.0)
		time_r = 0.0;
						{
							best_crit = e->life;
							best_enemy = e;
						}
						break;
					default:
						best_enemy = e;
						break;
				}
			}
		}
		if(best_enemy)
		{
			new CShot(game, best_enemy, this);
			time_r = shoot_time;
		}
	}

}

CFireTower::~CFireTower(void)
{
	if(fire_source != -1)
		game->fire.context->ClearSource(fire_source);
}


void CFireTower::Run(float time)
{
	CTower::Run(time);

	CEnemy *e;
	CEnemy *best_enemy;
	float best_crit;
	float e_dist;

	if(time_r == 0.0)
	{
		best_crit = -(float)INFINITY;
		best_enemy = 0;

		for(e=game->first_enemy; e; e=e->chain_next)
		{
			if(!e->run || e->dead)
				continue;

			e_dist = ((e->pos + e->aim_pos) - (pos + shoot_pos)).Len();
			if(e_dist <= range)
			{
				if(-e->slow_time > best_crit)
				{
					best_crit = -e->slow_time;
					best_enemy = e;
				}
			}
		}
		if(best_enemy)
		{
			new CShot(game, best_enemy, this);
			time_r = shoot_time;
		}
	}

	if(fire_source != -1)
	{
		if(!(game->selected_tower == this && game->game_mode == GAME_MODE_BUILD_TOWER))
		{
			game->fire.context->Source(fire_source, 50.0 * game->fire.speed, pos + Vec(0.0, 2.5, 0.0), 0.5);
			game->fire.context->Velocity(fire_source, Vec(0.0, 1.0, 0.0) * game->fire.speed, 0.3);
		}
		else
			game->fire.context->ClearSource(fire_source);
	}
}

