#include "allinc.h"

int display_width;
int display_height;

int fullscreen;
int aa_level;

const SDL_VideoInfo *video_info;
SDL_Surface *screen;
SDL_Rect screen_rect;

int audio_freq; 
Uint16 audio_format;
int audio_channels;
int audio_buffers;

CMusicPlayer *music_player;

OGLFT::Translucent *fonts[FONT_COUNT];

SDL_Surface *cursor, *rotate_cursor, *move_cursor;
GLuint cursor_gl, rotate_cursor_gl, move_cursor_gl;

CProgram *program;
