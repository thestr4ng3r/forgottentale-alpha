#include "allinc.h"

void CFlamable::SelectFireSource(void)
{
	int i;

	fire_source = -1;
	for(i=0; i<game->fire.max_src; i++)
	{
		if(game->fire.free_src[i] == 0)
		{
			fire_source = i;
			game->fire.free_src[i] = 1;
			break;
		}
	}

}
