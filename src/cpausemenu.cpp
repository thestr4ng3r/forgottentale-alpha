#include "allinc.h"

#define PAUSE_MENU_FADE_TIME	0.2

CPauseMenu::CPauseMenu(CGame *game, const char *file)
{
	this->game = game;
	sprite.LoadImage(file);
	sprite.SetPosition(display_width / 2 - sprite.Width() / 2, display_height / 2 - sprite.Height() / 2);
	mode = PAUSE_MENU_INVISIBLE;
	fade = 0.0;
}

void CPauseMenu::Run(float time)
{
	switch(mode)
	{
	case PAUSE_MENU_FADE_IN:
		fade += time / PAUSE_MENU_FADE_TIME;
		break;
	case PAUSE_MENU_FADE_OUT:
		fade -= time / PAUSE_MENU_FADE_TIME;
		break;
	}
	if(fade >= 1.0)
	{
		fade = 1.0;
		if(mode == PAUSE_MENU_FADE_IN)
			mode = PAUSE_MENU_VISIBLE;
	}
	if(fade <= 0.0)
	{
		fade = 0.0;
		if(mode == PAUSE_MENU_FADE_OUT)
			mode = PAUSE_MENU_INVISIBLE;
	}
}

void CPauseMenu::PutToGL(void)
{
	sprite.SetAlpha(fade);
	sprite.PutToGL();
}

void CPauseMenu::OnEvent(SDL_Event event)
{
	if(mode != PAUSE_MENU_VISIBLE)
		return;

	switch(event.type)
	{
	case SDL_KEYDOWN:
		switch(event.key.keysym.sym)
		{
		case SDLK_q:
			if(mode == PAUSE_MENU_VISIBLE)
				game->QuitGame();
			break;
		case SDLK_ESCAPE:
			mode = PAUSE_MENU_FADE_OUT;
			break;
		default:
			break;
		}
		break;
	}
}
