#include "allinc.h"

void CGame::InitGlobalGame(void)
{
}

CGame::CGame(CProgram *program)
{
	first_enemy = 0;
	first_tower = 0;
	first_shot = 0;
	map = 0;
	show_fps = 0;
	this->program = program;
	loading = false;
}

void CGame::InitSystem(void)
{
	//int i, j;
	//SDL_Color color = {255, 0, 0};

	// ---------------------- render ----------------------

	render.aa_level = (float)aa_level;
	if(render.aa_level <= 0.0)
		render.aa_level = 1.0;

	glGenTextures(1, &render.color_tex);
	glBindTexture(GL_TEXTURE_2D, render.color_tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, display_width * render.aa_level, display_height * render.aa_level, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &render.color_tex_dump);
	glBindTexture(GL_TEXTURE_2D, render.color_tex_dump);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, display_width * render.aa_level, display_height * render.aa_level, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &render.color_tex_temp);
	glBindTexture(GL_TEXTURE_2D, render.color_tex_temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, display_width * render.aa_level, display_height * render.aa_level, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &render.color_tex_blurred);
	glBindTexture(GL_TEXTURE_2D, render.color_tex_blurred);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, display_width * render.aa_level, display_height * render.aa_level, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &render.depth_tex);
	glBindTexture(GL_TEXTURE_2D, render.depth_tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, display_width * render.aa_level, display_height * render.aa_level, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &render.depth_tex_dump);
	glBindTexture(GL_TEXTURE_2D, render.depth_tex_dump);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, display_width * render.aa_level, display_height * render.aa_level, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffersEXT(1, &render.fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, render.fbo);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, render.color_tex, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, render.depth_tex, 0);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	PaintLoading();

	// ----------------------- blur -----------------------

	glGenFramebuffersEXT(1, &blur.fbo);

	PaintLoading();

	// ---------------------- shadow ----------------------

	glGenTextures(1, &shadow.tex);
	glBindTexture(GL_TEXTURE_2D, shadow.tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadow.width, shadow.height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffersEXT(1, &shadow.fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, shadow.fbo);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, shadow.tex, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);


	PaintLoading();
}

void CGame::InitGame(void)
{
	int i, j;

	first_enemy = 0;
	first_tower = 0;
	first_shot = 0;

	mouse_locked = 1;

	debug_mode = 0;

	debug_camera.speed = 0.08;
	debug_camera.rot_speed = 0.004;
	debug_camera.rot.Set(-M_PI / 2.0, 0.0);
	debug_camera.focus.Set(0.0, 0.0, 0.0);
	debug_camera.pos.Set(0.0, 0.0, 3.0);
	debug_camera.angle = 60.0;

	CalculateDebugCamera();

	camera.focus.Set(0.0, 0.0, 0.0);
	camera.dist = 17.0;
	camera.angle.Set(0.0, 0.6);
	camera.min_angle = camera.angle.y; //0.2;
	camera.max_angle = camera.angle.y; //M_PI / 2 - 0.6;
	camera.move_angle = camera.angle;
	camera.move_focus = camera.focus;
	camera.move_dist = camera.dist;
	camera.focus_speed = 0.01;
	camera.dist_speed = 1.0;
	camera.angle_speed.Set(0.003, 0.005);
	camera.min_dist = 13.0;
	camera.max_dist = 25.0;

	CalculateCamera();

	light.pos = Vec(-20.0, 40.0, 20.0);
	light.color = Vec(0.7, 0.7, 0.7);

	water.normal_tex = LoadGLTexture("./water/001.png");
	water.anim = 0.0;

	map = new CMap(this);
	map->LoadFromFile("./maps/dev/map.xml");



	PaintLoading();

	// ---------------------- water ----------------------

	water.width = display_width;
	water.height = display_height;

	glGenTextures(1, &water.tex);
	glBindTexture(GL_TEXTURE_2D, water.tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, water.width, water.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenRenderbuffersEXT(1, &water.rbo);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, water.rbo);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, water.width, water.height);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

	glGenFramebuffersEXT(1, &water.fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, water.fbo);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, water.tex, 0);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, water.rbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);


	PaintLoading();


	// -------------- field --------------

	field.max = Vec(-INFINITY, -INFINITY, -INFINITY);
	field.min = Vec(INFINITY, INFINITY, INFINITY);
	CVector v;
	for(i=0; i<map->ground_count; i++)
	{
		for(j=0; j<3; j++)
		{
			v = map->ground_t[i].v[j];
			field.max.x = max(v.x, field.max.x);
			field.max.y = max(v.y, field.max.y);
			field.max.z = max(v.z, field.max.z);
			field.min.x = min(v.x, field.min.x);
			field.min.y = min(v.y, field.min.y);
			field.min.z = min(v.z, field.min.z);
		}
	}
	field.scale.x = (float)field.size / (field.max.x - field.min.x);
	field.scale.y = (float)field.size / (field.max.z - field.min.z);
	field.data = new unsigned char[field.size * field.size];
	memset(field.data, 255, field.size * field.size * sizeof(unsigned char));

	glGenTextures(1, &field.color_tex);
	glBindTexture(GL_TEXTURE_2D, field.color_tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, field.size, field.size, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &field.depth_tex);
	glBindTexture(GL_TEXTURE_2D, field.depth_tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, field.size, field.size, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffersEXT(1, &field.fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, field.fbo);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, field.color_tex, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, field.depth_tex, 0);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	RenderField();

	map->FindShortestWay();


	PaintLoading();

	// -------------- templates --------------

	templates.towers.arrow.tower.LoadFromFile("./towers/arrow/mesh.tem");
	PaintLoading();
	templates.towers.arrow.shot.LoadFromFile("./towers/arrow/shot/mesh.tem");
	PaintLoading();
	templates.towers.slow.tower.LoadFromFile("./towers/slow/mesh.tem");
	PaintLoading();
	templates.towers.slow.shot.LoadFromFile("./towers/slow/shot/mesh.tem");
	PaintLoading();
	templates.towers.ice.tower.LoadFromFile("./towers/ice/mesh.tem");
	PaintLoading();
	templates.towers.ice.shot.LoadFromFile("./towers/ice/shot/mesh.tem");
	PaintLoading();
	templates.towers.fire.tower.LoadFromFile("./towers/fire/mesh.tem");
	PaintLoading();
	templates.towers.fire.shot.LoadFromFile("./towers/fire/shot/mesh.tem");
	PaintLoading();
	templates.towers.stone.tower.LoadFromFile("./towers/stone/mesh.tem");
	PaintLoading();


	templates.textures.particles.fire[0] = LoadGLTexture("./images/particles/fire/fire01.png");
	PaintLoading();
	templates.textures.particles.fire[1] = LoadGLTexture("./images/particles/fire/fire02.png");
	PaintLoading();
	templates.textures.particles.fire[2] = LoadGLTexture("./images/particles/fire/fire03.png");
	PaintLoading();
	templates.textures.particles.fire[3] = LoadGLTexture("./images/particles/fire/fire04.png");
	PaintLoading();
	templates.textures.particles.smoke = LoadGLTexture("./images/particles/fire/smoke.png");
	PaintLoading();

	templates.enemy.LoadFromFile("./enemies/pyramid/mesh.tem");
	PaintLoading();

	// -------------- music -----------------

	music_player->Init();
	PaintLoading();


	// -------------- rest -----------------

	fire.context = new CParticleContext(fire.max_src);
	particle_tex = CreateBlendSpriteTexture();
	PaintLoading();
	fire.speed = 3.0;
	fire.free_src = new int[fire.max_src];
	for(i=0; i<fire.max_src; i++)
		fire.free_src[i] = 0; // 0 = free
	PaintLoading();

	defeat_sprite.LoadImage("./images/defeat.png");
	defeat_sprite.SetPosition(display_width / 2 - defeat_sprite.Width() / 2, display_height / 2 - defeat_sprite.Height() * 0.65);
	PaintLoading();

	//printf("-----1\n");
	//fire.context->Print();

	mouse_buttons[0] = mouse_buttons[1] = mouse_buttons[2] = 0;

	mouse_cursor.SetImage(cursor_gl);
	mouse_cursor.SetSize(cursor->w, cursor->h);
	mouse_cursor.LockDestroying();
	mouse_cursor.SetPosition(screen_rect.w / 2, screen_rect.h / 2);

	selected_tower = 0;
	game_mode = GAME_MODE_NONE;

	//printf("-----2\n");
	//fire.context->Print();
	PaintLoading();
	game_hud = new CGameHUD(this);
	game_hud->Load();
	PaintLoading();

	pause_menu = new CPauseMenu(this, "./images/pause_menu.png");

	lifes = 20;
	mana = 100;

	defeat.anim = 0.0;
	defeat.defeat = 0;

	field.show = false;

	//printf("-----3\n");
	//fire.context->Print();

	SDL_WarpMouse(screen_rect.w / 2, screen_rect.h / 2);
	music_player->Play();
	PaintLoading();
}

const char *loading_anim_files[] = {	"images/loading_anim/0001.png",
										"images/loading_anim/0002.png",
										"images/loading_anim/0003.png",
										"images/loading_anim/0004.png",
										"images/loading_anim/0005.png",
										"images/loading_anim/0006.png",
										"images/loading_anim/0007.png",
										"images/loading_anim/0008.png",
										"images/loading_anim/0009.png",
										"images/loading_anim/0010.png" };

void CGame::Init(void)
{
	loading = true;
	loading_tick = SDL_GetTicks();
	loading_last_redraw = 0;
	loading_anim.LoadAnimation(10, loading_anim_files, 30.0);
	loading_anim.SetPosition(display_width - loading_anim.Width() - 10,
			display_height - loading_anim.Height() - 10);
	PaintLoading();

	printf("Initializing Game System...\n");
	InitSystem();


	printf("Initializing Game Data...\n");
	InitGame();
	printf("Initialisation finished\n");

	loading = false;
}


void CGame::Destroy(void)
{
	if(map)
		delete map;
	while(first_enemy)
		delete first_enemy;
	while(first_tower)
		delete first_tower;
	while(first_shot)
		delete first_shot;
}

CGame::~CGame(void)
{
	Destroy();
}
