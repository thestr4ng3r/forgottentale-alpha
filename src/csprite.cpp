#include "allinc.h"

SDL_Color GetPixelColor(SDL_Surface *surface, int x, int y)
{
	SDL_Color c;

	SDL_GetRGB(GetPixel(surface, x, y), surface->format, &c.r, &c.g, &c.b);

	return c;
}

Uint32 GetPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *)p;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *)p;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void DrawPixel(SDL_Surface *screen, int x, int y, Uint8 R, Uint8 G, Uint8 B)
{
    Uint32 color = SDL_MapRGB(screen->format, R, G, B);

    if ( SDL_MUSTLOCK(screen) ) {
        if ( SDL_LockSurface(screen) < 0 ) {
            return;
        }
    }
    switch (screen->format->BytesPerPixel) {
        case 1: { 
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
            *bufp = color;
        }
        break;

        case 2: { 
            Uint16 *bufp;

            bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
            *bufp = color;
        }
        break;

        case 3: { 
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
            if(SDL_BYTEORDER == SDL_LIL_ENDIAN) {
                bufp[0] = color;
                bufp[1] = color >> 8;
                bufp[2] = color >> 16;
            } else {
                bufp[2] = color;
                bufp[1] = color >> 8;
                bufp[0] = color >> 16;
            }
        }
        break;

        case 4: { 
            Uint32 *bufp;

            bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
            *bufp = color;
        }
        break;
    }
    if ( SDL_MUSTLOCK(screen) ) {
        SDL_UnlockSurface(screen);
    }
    SDL_UpdateRect(screen, x, y, 1, 1);
}


CSprite::CSprite(const char *file, int x, int y)
{
	gl_image = 0;

	if(file)
		LoadImage(file);

	this->x = x;
	this->y = y;

	alpha = 1.0;
}


CSprite::CSprite(int x, int y)
{
	this->x = x;
	this->y = y;

	alpha = 1.0;

	gl_image = 0;
}

CSprite::~CSprite(void)
{
	Destroy();
}


bool CSprite::LoadImage(const char *img)
{
	SetImage(LoadGLTexture(img, &width, &height));
	return gl_image != 0;
}

void CSprite::SetImage(GLuint img)
{
	gl_image = img;
}

void CSprite::PutToGL(int scale_width, int scale_height)
{
	int scale_width2 = scale_width != 0 ? scale_width : width;
	int scale_height2 = scale_height != 0 ? scale_height : height;

	PutImageToGL(gl_image, x, y, scale_width2, scale_height2, alpha);
}

void CSprite::DestroyGLImage(void)
{
	if(gl_image && !locked)
	{
		glDeleteTextures(1, &gl_image);

		gl_image = 0;
	}
}

void CSprite::Destroy(void)
{
	DestroyGLImage();
}


GLuint CreateGLImage(SDL_Surface *img)
{
	int x, y;
	Uint8 r, g, b, a;
	int w = img->w;
	int h = img->h;
	unsigned char data[w*h*4];
	unsigned char *d;
	GLuint handle;

	d = data;

	SDL_LockSurface(img);
	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{			
			SDL_GetRGBA(GetPixel(img, x, h-y), img->format, &r, &g, &b, &a);
			*d++ = r;
			*d++ = g;
			*d++ = b;
			*d++ = a;
		}
	}
	SDL_UnlockSurface(img);
	
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D, 0);
	return handle;
}

GLuint CreateSquareGLImage(SDL_Surface *img)
{
	int x, y, ox, oy;
	Uint8 r, g, b, a;
	int w = img->w;
	int h = img->h;
	int side = w > h ? w : h;
	int dist_x = (side - w) / 2;
	int dist_y = (side - h) / 2;
	unsigned char data[side*side*4];
	unsigned char *d;
	GLuint handle;

	d = data;

	SDL_LockSurface(img);
	for(y=0; y<side; y++)
	{
		for(x=0; x<side; x++)
		{	
			ox = x - dist_x;
			oy = y - dist_y;
			if(ox >= 0 && ox < w && oy >= 0 && oy < h)
				SDL_GetRGBA(GetPixel(img, ox, oy), img->format, &r, &g, &b, &a);
			else
				r = g = b = a = 0;
			*d++ = r;
			*d++ = g;
			*d++ = b;
			*d++ = a;
		}
	}
	SDL_UnlockSurface(img);
	
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, side, side, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, side, side, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D, 0);
	return handle;

}

void ConvertScreenToGL(SDL_Surface *screen, GLuint &gl_screen)
{
	if(gl_screen)
		glDeleteTextures(1, &gl_screen);
	gl_screen = CreateGLImage(screen);
}

void PutImageToGL(GLuint image, int x, int y, int width, int height, float alpha)
{
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glActiveTextureARB(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
 	glBindTexture(GL_TEXTURE_2D, image);
	Blend();

	glColor4f(1.0, 1.0, 1.0, alpha);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1.0); glVertex2i(x, y);
	glTexCoord2f(0, 0); glVertex2i(x, y + height);
	glTexCoord2f(1.0, 0); glVertex2i(x + width, y + height);
	glTexCoord2f(1.0, 1.0); glVertex2i(x + width, y);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}

void Set2DMatrix(void)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, (float)screen_rect.w, (float)screen_rect.h, 0.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
}

void Blend(void)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
