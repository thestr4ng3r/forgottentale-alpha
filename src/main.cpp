#include "allinc.h"

void ExitError(const char *text);
void InitSDL(void);
void InitAudio(void);
void InitGL(void);
int LoadValuesFromFile(const char *file, int count, const char *names[], char seperator, int *values[], const char *header = 0, int max_value_len = 15);
void LoadSettings(const char *file);
void MainLoop(void);
void OnEvent(SDL_Event event);
void OnKeyDown(SDL_KeyboardEvent event);
void OnKeyUp(SDL_KeyboardEvent event);
void OnMouseMotion(SDL_MouseMotionEvent event);
void CalculateCamFocus(void);
void Set2dMatrix(void);
void Paint(void);
void Quit(void);


SDL_Surface *icon;

int main(int argc, char *argv[])
{
	srand(time(0));
	InitSDL();
	InitGL();
	CProgram::InitGlobal();
	program = new CProgram();
	program->Init();
	
	while(1)
	{
	    MainLoop();
	}
	return 0;
}

void ExitError(const char *text)
{
	printf("%s", text);
	exit(-1);
}

void InitSDL(void)
{
	int bpp;
	int flags;
	SDL_PixelFormat format;

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	      ExitError("Video initialisation failed");

	atexit(Quit);

	InitAudio();

	LoadSettings("settings");

	video_info = SDL_GetVideoInfo();
	
	if(!video_info)
	      ExitError("Video query failed");
	
	bpp = video_info->vfmt->BitsPerPixel;
	
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	
	flags = SDL_OPENGL;
	if(fullscreen)
		flags |= SDL_FULLSCREEN;
	

	if((screen = SDL_SetVideoMode(display_width, display_height, bpp, flags)) == 0)
	     ExitError("Video mode set failed\n");

	SDL_WM_SetCaption("Forgotten Tale", "./images/icon.png");


	//icon = IMG_Load();
	//if(icon)
	//	SDL_WM_SetIcon(icon, NULL);

	format = *screen->format;

	screen_rect.x = screen_rect.y = 0;
	screen_rect.w = screen->w; screen_rect.h = screen->h;

	SDL_ShowCursor(SDL_DISABLE);
}

void MusicDone(void)
{
	music_player->Play();
}

void InitAudio(void)
{
	audio_freq = 22050;
	audio_format = AUDIO_S16;
	audio_channels = 2;
	audio_buffers = 4096;

	if(Mix_OpenAudio(audio_freq, audio_format, audio_channels, audio_buffers) < 0)
		ExitError("Audio Initialisation failed\n");

	music_player = new CMusicPlayer();
	Mix_HookMusicFinished(MusicDone);
}

void InitGL(void)
{
	printf("Supported OpenGL version: %s\n", glGetString(GL_VERSION));

#ifdef _WIN32
	glewInit();
#endif

	glShadeModel(GL_SMOOTH);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glClearColor(0, 0, 0, 0);
	glViewport(0, 0, display_width, display_height);

	ilInit();
	iluInit();

	face_shader.Init();
	water_shader.Init();
	display_shader.Init();
	environment_shader.Init();
	field_shader.Init();
	blur_shader.Init();
}

int LoadValuesFromFile(const char *file, int count, const char *names[], char seperator, int *values[], const char *header, int max_value_len)
{
	int h, i, finished;
	char *name;
	char *value;
	char *f_header;
	char c;
	int f_values[count];
	int f_values_read[count];
	int f_value_id;
	int max_name_len = 0;

	for(i=0; i<count; i++)
	{
		f_values[i] = *values[i];
		max_name_len = (int)strlen(names[i]) > max_name_len ? (int)strlen(names[i]) : max_name_len;
		f_values_read[i] = 0;
	}
	
	h = open(file, O_RDONLY);
	if(h < 0)
	{
		return FILE_READ_NOT_EXISTING;
	}
	if(header)
	{
		f_header = new char[strlen(header) + 1];
		read(h, f_header, strlen(header));
		if(strcmp(header, HEADER) != 0)
			return FILE_READ_WRONG_HEADER;
	}

	while(1)		
	{
		c = 0;
		finished = 0;
		name = new char[max_name_len + 1];
		for(i=0; i<max_name_len + 1; i++)
		{
			name[i] = c = 0;

			if(!read(h, &c, 1))
			{
				finished = 1;
				break;				
			}
			
			if(c != seperator)
				name[i] = c;
			else
				break;
		}

		if(finished)
			break;

		f_value_id = -1;
		for(i=0; i<count; i++)
		{
			if(strcmp(name, names[i]) == 0)	
			{
				f_value_id = i;
				break;
			}
		}


		if(f_value_id == -1)
			continue;

		value = new char[max_value_len + 1];
		for(i=0; 1; i++)
		{
			if(i<max_value_len)
				value[i] = c = 0;
			
			if(!read(h, &c, 1))
			{
				if(i>0)
				{
					finished = 1;
					break;
				}
				else
					return FILE_READ_UNEXPECTED_EOF;
			}

			if(c == '\n')
				break;
			if(i < max_value_len)
				value[i] = c;
		}

		f_values[f_value_id] = atoi(value);
		f_values_read[f_value_id] = 1;
	}
	for(i=0; i<count; i++)
		if(!f_values_read[i])
			return FILE_READ_NAME_NOT_AVAILABLE;
		
	for(i=0; i<count; i++)
		*(values[i]) = f_values[i];

	return 0;
}

void LoadSettings(const char *file)
{
	int result, h;
	const char *names[] = {"fullscreen", "screen-width", "screen-height", "anti-aliasing"};
	int *values[] = { &fullscreen,  &display_width,  &display_height, &aa_level };
	char *temp;
	
	result = LoadValuesFromFile(file, 4, names, ':', values, HEADER);

	switch(result)
	{
		case FILE_READ_NOT_EXISTING:
			h = open(file, O_WRONLY | O_CREAT | O_TRUNC, 0666);
			if(!(h < 0))
			{
				write(h, HEADER, strlen(HEADER));
	
				temp = new char[115];
				snprintf(temp, 114, "fullscreen:%d\n", PRESET_FULLSCREEN);
				write(h, temp, strlen(temp));

				temp = new char[115];
				snprintf(temp, 114, "screen-width:%d\n", PRESET_DISPLAY_WIDTH);
				write(h, temp, strlen(temp));

				temp = new char[115];
				snprintf(temp, 114, "screen-height:%d\n", PRESET_DISPLAY_HEIGHT);
				write(h, temp, strlen(temp));
				
				temp = new char[115];
				snprintf(temp, 114, "anti-aliasing:%d\n", PRESET_ANTI_ALIASING_LEVEL);
				write(h, temp, strlen(temp));
			}
			break;
		case FILE_READ_UNEXPECTED_EOF:
			printf("unexpected eof while reading settings\n");
			break;
		case FILE_READ_WRONG_HEADER:
			printf("incorrect header of settings file!\n");
			break;
	}

	if(result < 0)
	{
		printf("read error %d\n", result);
		fullscreen = PRESET_FULLSCREEN;
		display_width = PRESET_DISPLAY_WIDTH;
		display_height = PRESET_DISPLAY_HEIGHT;
		aa_level = PRESET_ANTI_ALIASING_LEVEL;
	}
}

void MainLoop(void)
{
	SDL_Event event;
	Uint32 start_time, time;

	start_time = SDL_GetTicks();

	while(SDL_PollEvent(&event))
		OnEvent(event);


	program->Run();

	Paint();

	time = SDL_GetTicks() - start_time;
	if(time < MIN_FRAME_TIME)
	{
		SDL_Delay(MIN_FRAME_TIME - time);
		program->last_tick = (float)(time + MIN_FRAME_TIME - time) / 1000.0;

	}
	else
		program->last_tick = (float)time / 1000.0;
}

void OnEvent(SDL_Event event)
{
	program->OnEvent(event);
	
	switch(event.type)
	{
		case SDL_QUIT:
			exit(0);
			break;
	}
}

GLuint CreateBlendSpriteTexture(void)
{
	unsigned char data[512*512*4];
	int x, y;
	double dx, dy, v;
	unsigned char c;
	unsigned char *d;
	GLuint handle;

	d = data;

	for(y=0; y<512; y++)
	{
		dy = (double)y / 511.0 * 2.0 - 1.0;
		for(x=0; x<512; x++)
		{
			dx = (double)x / 511.0 * 2.0 - 1.0;
			v = 1.0 - (dx * dx + dy * dy);
			v *= 1.25;

			if(v > 1.0)
				v = 1.0;
			else if(v < 0.0)
				v = 0.0;

			c = (unsigned char)(v * 255);
			*d++ = 255;
			*d++ = 255;
			*d++ = 255;
			*d++ = c;
		}
	}

	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, 512, 512, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D, 0);
	return handle;
}

GLuint CreateWhiteBlendSpriteTexture(void)
{
	unsigned char data[512*512*4];
	int x, y;
	double dx, dy, v;
	unsigned char c;
	unsigned char *d;
	GLuint handle;

	d = data;

	for(y=0; y<512; y++)
	{
		dy = (double)y / 511.0 * 2.0 - 1.0;
		for(x=0; x<512; x++)
		{
			dx = (double)x / 511.0 * 2.0 - 1.0;
			v = 1.0 - (dx * dx + dy * dy);
			v *= 1.25;

			if(v > 1.0)
				v = 1.0;
			else if(v < 0.0)
				v = 0.0;

			c = (unsigned char)(v * 255);
			*d++ = 255;
			*d++ = 255;
			*d++ = 255;
			*d++ = c;
		}
	}

	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, 512, 512, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D, 0);
	return handle;
}

void Paint(void)
{
	program->Paint();
	/*Set2DMatrix();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	gl_fonts[0]->FaceSize(72);
	gl_fonts[0]->Render("Hello World", -1, FTPoint(100.0, 100.0, 0.0));
	glFlush();
	SDL_GL_SwapBuffers();*/

}

void Quit(void)
{
	Mix_CloseAudio();
	SDL_Quit();
}


