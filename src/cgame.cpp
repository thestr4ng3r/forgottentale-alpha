#include "allinc.h"

#define DEBUG_WP_SPEED 0.05

void CGame::ProcessMouse(void)
{
	int mouse_x, mouse_y;

	if(mouse_locked)
	{
		SDL_GetMouseState(&mouse_x, &mouse_y);
		mouse_x -= screen_rect.w / 2;
       		mouse_y -= screen_rect.h / 2;
		SDL_WarpMouse(screen_rect.w / 2, screen_rect.h / 2);
		SDL_ShowCursor(SDL_DISABLE);
	}
	else
	{
		mouse_x = mouse_y = 0;
		SDL_ShowCursor(SDL_ENABLE);
	}

	if(debug_mode)
	{
		debug_camera.rot.x += debug_camera.rot_speed * mouse_x;
		debug_camera.rot.y -= debug_camera.rot_speed * mouse_y;
	}
	else
	{
		if(!mouse_buttons[2] && !mouse_buttons[1]) // normal
		{
			mouse_cursor.SetImage(cursor_gl);
			mouse_cursor.SetSize(cursor->w, cursor->h);
			mouse_cursor.Move(mouse_x, mouse_y);

			if(mouse_cursor.X() < 0)
				camera.move_focus += camera.focus_horz * (float)(mouse_cursor.X()) * camera.focus_speed;

			if(mouse_cursor.Y() < 0)
				camera.move_focus += camera.focus_vert * (float)(mouse_cursor.Y()) * camera.focus_speed;

			if(mouse_cursor.X() > screen_rect.w)
				camera.move_focus += camera.focus_horz * (float)(mouse_cursor.X() - (screen_rect.w)) * camera.focus_speed;

			if(mouse_cursor.Y() > screen_rect.h)
				camera.move_focus += camera.focus_vert * (float)(mouse_cursor.Y() - (screen_rect.h)) * camera.focus_speed;
		}
		else if(mouse_buttons[2]) // move
		{
			mouse_cursor.SetImage(move_cursor_gl);
			mouse_cursor.SetSize(move_cursor->w, move_cursor->h);
			camera.move_focus += camera.focus_horz * mouse_x * camera.focus_speed;
			camera.move_focus += camera.focus_vert * mouse_y * camera.focus_speed;
		}
		else if(mouse_buttons[1]) // rotate
		{
			mouse_cursor.SetImage(rotate_cursor_gl);
			mouse_cursor.SetSize(rotate_cursor->w, rotate_cursor->h);
			camera.move_angle.x -= mouse_x * camera.angle_speed.x;
			camera.move_angle.y += mouse_y * camera.angle_speed.y;
		}
		camera.move_focus.x = min(max(camera.move_focus.x, map->minx), map->maxx);
		camera.move_focus.z = min(max(camera.move_focus.z, map->miny), map->maxy);
	}
	

}

void CGame::Run(void)
{
	Uint8 *keys;
	CVector side, up_vec;
	CEnemy *e;
	CTower *t;
	CShot *s;
	CVector v;

	ProcessMouse();

	keys = SDL_GetKeyState(NULL);

	if(debug_mode)
	{
		if(debug_camera.rot.y > M_PI / 2 - 0.01)
			debug_camera.rot.y = M_PI / 2 - 0.01;
		if(debug_camera.rot.y < -M_PI / 2 + 0.01)
			debug_camera.rot.y = -M_PI / 2 + 0.01;

		side = Cross((debug_camera.focus - debug_camera.pos), Vec(0.0, 1.0, 0.0));
		up_vec = Cross(side, (debug_camera.focus - debug_camera.pos));
		up_vec.Normalize();
		side.Normalize();

		if(keys[SDLK_c])
			debug_camera.angle *= 0.98;
		if(keys[SDLK_v])
			debug_camera.angle /= 0.98;
		
		float cam_speed = keys[SDLK_LSHIFT] ? debug_camera.speed * 6.0 : debug_camera.speed;

		if(keys[SDLK_w])
			debug_camera.pos += (debug_camera.focus - debug_camera.pos) * cam_speed;
		if(keys[SDLK_a])
			debug_camera.pos -= side * cam_speed;
		if(keys[SDLK_s])
			debug_camera.pos -= (debug_camera.focus - debug_camera.pos) * cam_speed;
		if(keys[SDLK_d])
			debug_camera.pos += side * cam_speed;
		if(keys[SDLK_PAGEUP])
			debug_camera.pos += up_vec * cam_speed;
		if(keys[SDLK_PAGEDOWN])
			debug_camera.pos -= up_vec * cam_speed;

		CalculateDebugCamera();
	}
	else
	{
		if(game_mode == GAME_MODE_PAUSE)
		{
			pause_menu->Run(program->last_tick);
			if(pause_menu->GetMode() == PAUSE_MENU_INVISIBLE)
				game_mode = GAME_MODE_NONE;
		}

		if(game_mode != GAME_MODE_PAUSE) // "else" would be skipped also if the mode was set in if-part
		{
			if(!(camera.move_focus == camera.focus))
				camera.focus += (camera.move_focus - camera.focus) * 0.15;

			if(camera.move_dist < camera.min_dist)
				camera.move_dist = camera.min_dist;
			if(camera.move_dist > camera.max_dist)
				camera.move_dist = camera.max_dist;

			if(camera.move_dist != camera.dist)
				camera.dist += (camera.move_dist - camera.dist) * 0.15;

			if(camera.move_angle.y < camera.min_angle)
				camera.move_angle.y = camera.min_angle;

			if(camera.move_angle.y > camera.max_angle)
				camera.move_angle.y = camera.max_angle;

			if(!(camera.move_angle == camera.angle))
				camera.angle += (camera.move_angle - camera.angle) * 0.4;

			if(mouse_cursor.X() < 0)
				mouse_cursor.SetX(0);
			if(mouse_cursor.Y() < 0)
				mouse_cursor.SetY(0);
			if(mouse_cursor.X() > screen_rect.w)
				mouse_cursor.SetX(screen_rect.w);
			if(mouse_cursor.Y() > screen_rect.h)
				mouse_cursor.SetY(screen_rect.h);

			switch(game_mode)
			{
				case GAME_MODE_BUILD_TOWER:
					if(PickFreeBuildPlace(mouse_cursor.X(), mouse_cursor.Y(), selected_tower, &v, &selected_tower->valid_pos))
						selected_tower->pos = v;
					break;
			}

			CEnemy *next_e;
			for(e=first_enemy; e; e=next_e)
			{
				next_e = e->chain_next;
				if(e->to_delete)
				{
					for(s=first_shot; s; s=s->chain_next)
						if(s->aim == e)
							s->to_delete = 1;
					delete e;
				}
			}
			for(e=first_enemy; e; e=e->chain_next)
				e->Run(program->last_tick);


			CShot *next_s;
			for(s=first_shot; s; s=next_s)
			{
				next_s = s->chain_next;
				if(s->to_delete)
					delete s;
			}

			for(s=first_shot; s; s=s->chain_next)
				s->Run(program->last_tick);

			for(t=first_tower; t; t=t->chain_next)
			{
				if(selected_tower == t && game_mode == GAME_MODE_BUILD_TOWER)
					continue;
				t->Run(program->last_tick);
			}

			if(lifes < 0)
				lifes = 0;

			if(lifes == 0)
				defeat.defeat = 1;

			if(defeat.defeat)
				defeat.anim = min(defeat.anim + program->last_tick, 5.0);

			water.anim += program->last_tick;

			CalculateCamera();
			game_hud->Refresh();
		}
	}
	if(!debug_mode)
		CalculateParticles(program->last_tick);
}

void CGame::CalculateDebugCamera(void)
{
    debug_camera.focus.Set(cos(debug_camera.rot.x) * cos(debug_camera.rot.y),
        sin(debug_camera.rot.y) , sin(debug_camera.rot.x) * cos(debug_camera.rot.y));
    debug_camera.focus += debug_camera.pos;
}

void CGame::CalculateCamera(void)
{
	CVector v;

	v.Set(	sin(camera.angle.x) * sin(camera.angle.y),
		cos(camera.angle.y),
		cos(camera.angle.x) * sin(camera.angle.y));
	v.Normalize();
	camera.focus_horz = Cross(Vec(0.0, 1.0, 0.0), v);
	camera.focus_horz.Normalize();
	camera.focus_vert = Cross(camera.focus_horz, Vec(0.0, 1.0, 0.0));
	v *= camera.dist;
	camera.pos = camera.focus + v;
}

void CGame::CalculateParticles(float time)
{
	fire.context->Range(Vec(-20.0, -50.0, -20.0), Vec(20.0, 20.0, 20.0));
	fire.context->Gravity(Vec(0.0, 0.3, 0.0) * fire.speed);
	fire.context->LifeTime(1.2, 1.6);
	fire.context->Textures(templates.textures.particles.fire, FIRE_TEXTURES);
	fire.context->Move(time);
}

void CGame::OnEvent(SDL_Event event)
{
	if(game_mode == GAME_MODE_PAUSE)
	{
		pause_menu->OnEvent(event);
		return;
	}

	switch(event.type)
	{
		case SDL_KEYDOWN:
			OnKeyDown(event.key);
			break;

		case SDL_KEYUP:
			OnKeyUp(event.key);
			break;

		case SDL_MOUSEMOTION:
			OnMouseMotion(event.motion);
			break;

		case SDL_MOUSEBUTTONDOWN:
			OnMouseButtonDown(event.button);
			break;

		case SDL_MOUSEBUTTONUP:
			OnMouseButtonUp(event.button);
			break;
	}
}

void CGame::OnMouseDown(int x, int y)
{
	mouse_locked = 1;
	switch(game_mode)
	{
		case GAME_MODE_BUILD_TOWER:
			CompleteBuildTower();
			break;
		default:
			PickTower(x, y);
			break;
	}

	game_hud->OnMouseDown(x, y);
}


int CGame::PickMouseV(int x, int y, CVector *v)
{	
	float depth[1];
	GLdouble modelm[16], projm[16], pos[3];
	GLint view[4];
	CVector ret;

	CVector cam_pos = GetCamPos();
	CVector focus = GetFocus();

	UseNoShader();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK);
	glViewport(0, 0, display_width, display_height);
	glGetIntegerv(GL_VIEWPORT, view);

 	glMatrixMode(GL_PROJECTION);
	//glPushMatrix();
 	glLoadIdentity();
 	gluPerspective(60, (double)display_width / (double)display_height, 0.1, 100.0);
 	glMatrixMode(GL_MODELVIEW);
	//glPushMatrix();
	glLoadIdentity();
	gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, focus.x, focus.y, focus.z, 0.0, 1.0, 0.0);
	current_cam_pos = cam_pos; current_focus = focus;
	glEnable(GL_DEPTH_TEST);

	glGetDoublev(GL_MODELVIEW_MATRIX, modelm);
	glGetDoublev(GL_PROJECTION_MATRIX, projm);

	map->PutGroundToGL();
	
	//glPopMatrix();
	//glMatrixMode(GL_PROJECTION);
	//glPopMatrix();

	glReadPixels(x, display_height - y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
	if(depth[0] == 1.0)
		return 0;
	gluUnProject(x, display_height - y, depth[0], modelm, projm, view, &pos[0], &pos[1], &pos[2]);
	ret.x = (float)pos[0];
	ret.y = (float)pos[1];
	ret.z = (float)pos[2];

	*v = ret;

	return 1;
}

int CGame::PickFreeBuildPlace(int x, int y, CTower *t, CVector *v, int *valid_pos)
{
	CVector p;
	CVector2 tp;
	int valid;
	int  w, h;
	int xi, yi;
	unsigned char val;

	valid = 1;

	if(!PickMouseV(x, y, &p))
		return 0;

	if(valid)
	{
		tp = field.GetTexCoord(p) * field.size;
		w = 2 * t->build_radius * field.scale.x;
		h = 2 * t->build_radius * field.scale.y;
		x = tp.x - w / 2;
		y = tp.y - h / 2;

		for(xi=max(x, 0); xi<x+w; xi++)
		{
			for(yi=max(y, 0); yi<y+h; yi++)
			{
				if(xi >= (int)field.size || yi >= (int)field.size)
					continue;
				val = field.data[xi+yi*field.size];
				if(val == 0)
					continue;
				if(val < 0.25 * 255)
				{
					valid = 0;
					break;
				}
			}
			if(!valid)
				break;
		}
	}

	*v = p;
	*valid_pos = valid;
	return 1;
}

void CGame::CancelBuild(void)
{
	if(game_mode != GAME_MODE_BUILD_TOWER)
		return;

	delete selected_tower;
	selected_tower = 0;
	game_mode = GAME_MODE_NONE;
}

void CGame::CreateEnemy(void)
{
	new CEnemy(this);
}

void CGame::OnKeyDown(SDL_KeyboardEvent event)
{
	switch(event.keysym.sym)
	{
		case SDLK_e:
			CreateEnemy();
			break;
		case SDLK_F1:
			debug_mode = !debug_mode;
			break;
		case SDLK_F2:
			field.show = !field.show;
			break;
		case SDLK_F3:
			mouse_locked = 0;
			break;
		case SDLK_F4:
			show_fps = !show_fps;
			break;
		case SDLK_F5:
			map->FindShortestWay();
			break;
		case SDLK_ESCAPE:
			if(debug_mode)
				break;
			if(game_mode == GAME_MODE_BUILD_TOWER)
				CancelBuild();
			else
			{
				game_mode = GAME_MODE_PAUSE;
				pause_menu->MakeVisible();
			}
			break;
		default:
			return;
	}
}

void CGame::OnKeyUp(SDL_KeyboardEvent event)
{
}

void CGame::OnMouseMotion(SDL_MouseMotionEvent event)
{
}

void CGame::OnMouseButtonDown(SDL_MouseButtonEvent event)
{
	switch(event.button)
	{
		case SDL_BUTTON_WHEELUP:
			camera.move_dist -= camera.dist_speed;
			break;
		case SDL_BUTTON_WHEELDOWN:
			camera.move_dist += camera.dist_speed;
			break;
		case SDL_BUTTON_LEFT:
			mouse_buttons[0] = 1;
			OnMouseDown(mouse_cursor.X(), mouse_cursor.Y());
			break;
		case SDL_BUTTON_MIDDLE:
			mouse_buttons[1] = 1;
			break;
		case SDL_BUTTON_RIGHT:
			mouse_buttons[2] = 1;
			break;
	}
}

void CGame::OnMouseButtonUp(SDL_MouseButtonEvent event)
{
	switch(event.button)
	{
		case SDL_BUTTON_LEFT:
			mouse_buttons[0] = 0;
			break;
		case SDL_BUTTON_MIDDLE:
			mouse_buttons[1] = 0;
			break;
		case SDL_BUTTON_RIGHT:
			mouse_buttons[2] = 0;
			break;
	}

}

void CGame::QuitGame(void)
{
	exit(0);
}


unsigned char CGame::GetFieldNodeValue(CVector v)
{
	CVector2 coord = field.GetTexCoord(v) * field.size;
	int x = (int)coord.x;
	int y = (int)coord.y;

	return field.data[x+y*field.size];
}

void CGame::PickTower(int x, int y)
{
	GLuint buff[64] = {0};
 	GLint hits;
	CVector cam_pos = GetCamPos();
    CVector _focus = GetFocus();
    GLint view[4] = { camera.viewport[0], camera.viewport[1],
    		camera.viewport[2] / render.aa_level, camera.viewport[3] / render.aa_level };

	int i;

	int name;
	CTower *t;

	int nearest_depth;
	int nearest_name;
	int depth;

	face_shader.BindShader();
	shader_enabled = 0;
 	glSelectBuffer(64, buff);
 	glRenderMode(GL_SELECT);
 	glInitNames();
 	glPushName(0);
 	glMatrixMode(GL_PROJECTION);
 	glPushMatrix();
 		glLoadIdentity();
 		gluPickMatrix(x, display_height - y, 1.0, 1.0, view);
		gluPerspective(60.0, (double)display_width / (double)display_height, 0.1, 100.0);
 		glMatrixMode(GL_MODELVIEW);

		glLoadIdentity();
		gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, _focus.x, _focus.y, _focus.z, 0.0, 1.0, 0.0);
		current_cam_pos = cam_pos; current_focus = _focus;

		for(t=first_tower, name=0; t; t=t->chain_next, name++)
		{
			t->name = name;
			glLoadName(name);
			t->PutToGL();
		}

		glMatrixMode(GL_PROJECTION);
 	glPopMatrix();
 	glMatrixMode(GL_MODELVIEW);
	shader_enabled = 1;
	hits = glRenderMode(GL_RENDER);
	
	if(hits == 0)
	{
		selected_tower = 0;
		game_mode = GAME_MODE_NONE;
		return;
	}

	nearest_depth = (int)INFINITY;
	nearest_name = -1;
	for(i=0; i<hits; i++)
	{
		depth = buff[i * 4 + 1];
		if(depth < nearest_depth)
		{
			nearest_depth = depth;
			nearest_name = buff[i * 4 + 3];
		}
	}

	if(nearest_name == -1)
		return;

	for(t=first_tower; t; t=t->chain_next)
	{
		if(t->name == nearest_name)
		{
			selected_tower = t;
			game_mode = GAME_MODE_SELECTED_TOWER;
			break;
		}
	}
}

void CGame::InitBuildTower(int t)
{
	if(game_mode == GAME_MODE_BUILD_TOWER)
		return;

	if(TowerPrice(t) > mana)
		return;

	selected_tower = CTower::CreateTower(this, t);
	game_mode = GAME_MODE_BUILD_TOWER;
}


bool CGame::CompleteBuildTowerCheck(void)
{
	CEnemy *e;
	bool way_possible;

	if(!selected_tower->valid_pos)
		return false;


	if(map->FindShortestWay(true, true) == 0) // check map way
		return false;

	way_possible = true;

	for(e=first_enemy; e; e=e->chain_next) // check enemy ways
	{
		if((e->way_temp = map->FindShortestWay(false, false, e->pos)) == 0)
		{
			way_possible = false;
			break;
		}
	}

	if(!way_possible)
	{                   // if it does not work, delete the calculated ways
		delete map->way_temp;
		map->way_temp = 0;

		for(e=first_enemy; e; e=e->chain_next)
		{
			if(e->way_temp != 0)
			{
				delete e->way_temp;
				e->way_temp = 0;
			}
		}

		return false;
	}

	return true;
}

void CGame::CompleteBuildTower(void)
{
	CEnemy *e;

	if(!CompleteBuildTowerCheck())
	{
		selected_tower->ColorFade(Vec(1.0, 0.0, 0.0), 1.5);
		return;
	}

	map->way = map->way_temp;
	map->way_temp = 0;

	for(e=first_enemy; e; e=e->chain_next)
	{
		e->way = e->way_temp;
		e->way_temp = 0;
		e->way_dist = 0.0;
	}

	mana -= TowerPrice(selected_tower->type);
	selected_tower = 0;
	game_mode = GAME_MODE_NONE;
}

int CGame::TowerPrice(int t)
{
	switch(t)
	{
		case TOWER_TYPE_ARROW:
			return templates.towers.arrow.price;
		case TOWER_TYPE_SLOW:
			return templates.towers.slow.price;
		case TOWER_TYPE_ICE:
			return templates.towers.slow.price;
		case TOWER_TYPE_FIRE:
			return templates.towers.fire.price;
		case TOWER_TYPE_STONE:
			return templates.towers.stone.price;
		default:
			return 0;
	}
}

CVector2 CGame::field::GetTexCoord(CVector v)
{
	CVector2 r;
	r.x = (v.x - min.x) / (max.x - min.x);
	r.y = 1.0 - (v.z - min.z) / (max.z - min.z);
	return r;
}








